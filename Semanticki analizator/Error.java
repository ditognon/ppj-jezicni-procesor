public class Error {
    private boolean error;

    public Error() {
        error = false;
    }

    public void set() {
        error = true;
    }

    public boolean get() {
        return error;
    }
}
