import java.io.*;
import java.util.*;
import java.nio.charset.Charset;

//SA KOMENTAROM TESTIRATI SU OZNACENE SVE STVARI KOJE JE POTREBNO TESTIRATI PRIJE PREDAJE
//SA KOMENTAROM VIDJETI SU OZNACENE SVE STVARI KOJE TREBA SKUZITI PRIJE PREDAJE

class SemantickiAnalizator {
	static ArrayList<String> unarniOperatori = new ArrayList<>();
	static int unutarPetlje = 0;
	static String currFunc = "##none##";
	static Node korijen;
	static TablicaZnakova trenutna = new TablicaZnakova(null);
	static ArrayList<TablicaZnakova> sveTablice = new ArrayList<TablicaZnakova>();

	// Pointer koji uvijek pokazuje na globalnu i ne mijenja se
	static TablicaZnakova globalna = trenutna;

	// Pomocna funkcija za odredivanje na kojoj dubini u stablu se nalazi odredeni
	// cvor
	static int razina(String tekst) {
		int counter = 0;
		char[] array = tekst.toCharArray();
		for (int i = 0; i < array.length; i++) {
			if (array[i] != ' ') {
				return counter;
			} else {
				counter++;
			}
		}
		return counter;
	}

	//Printa zavrni znak u obliku oznaka(redak, ime), npr. IDN(2,x)
	static String pZ(Node zavrsni) {
		String[] temp = zavrsni.name.split(" ");
		if (temp.length > 1) {
			return temp[0] + "(" + temp[1] + "," + temp[2] + ")";
		}
		return "\nNije predan zavrsni znak: " + zavrsni.name + "\n";
	}

	//vraca true ako je niz znakova ispravan
	static boolean ispravanNiz(String niz) {
        char[] znakovi = niz.toCharArray();
        boolean prefiksiran = false;

		for (int i = 0; i < znakovi.length; i++) {
			if(prefiksiran) {
                prefiksiran = false;
                if(znakovi[i] == 't' || znakovi[i] == 'n' || znakovi[i] == '0' || znakovi[i] == '\'' ||
                znakovi[i] == '\"' || znakovi[i] == '\\') {

                }else {
                    return false;
                }
            }else {
                if(znakovi[i] == '\\') prefiksiran = true;
                else {
                    if(!Charset.forName("US-ASCII").newEncoder().canEncode(Character.toString(znakovi[i]))) {
                        return false;
                    }
                }
            }
		}

		return true;
	}

	// Trazi deklariranu varijablu u tablici znakova i postavlja joj tip i
	// l-izraz(KORISTI SE ZA IDN)
	static boolean provjeriTablicu(TablicaZnakova tablica, Node trazeniCvor) {
		String trazenoIme = trazeniCvor.name.split(" ")[2];
		while (tablica != null && !tablica.deklariraneVar.containsKey(trazenoIme)) {
			tablica = tablica.roditelj;
		}

		if (tablica != null) {
			String string = tablica.deklariraneVar.get(trazenoIme);

			if(string.startsWith("DEK") || string.startsWith("DEF")) {
				trazeniCvor.podaciNodea.tip = "funkcija(" + string.split("%%")[1] + ")";
			}else {
				trazeniCvor.podaciNodea.tip = string;
			}


			if (trazeniCvor.podaciNodea.tip.equals("int") || trazeniCvor.podaciNodea.tip.equals("char")) {
				trazeniCvor.podaciNodea.lIzraz = 1;
			} else {
				trazeniCvor.podaciNodea.lIzraz = 0;
			}
			return true;
		}

		return false;
	}

	static TablicaZnakova provjeriTablicu(TablicaZnakova tablica, String trazenoIme) {
		while (tablica != null && !tablica.deklariraneVar.containsKey(trazenoIme)) {
			tablica = tablica.roditelj;
		}

		if (tablica != null) {
			return tablica;
		}

		return null;
	}

	//True ako je A moguce eksplicitno castati na B - ne provjerava raspon 
	static boolean expCastable(String A, String B) {
		if(castable(A, B)) return true;
		else if(A.equals("int") && B.equals("char")) {
			return true;
		 }else if(A.equals("int") && B.equals("const(char)")) {
			 return true;
		 }else if(A.equals("const(int)") && B.equals("char")) {
			return true;
		}else if(A.equals("const(int)") && B.equals("const(char)")) {
			return true;
		}

		return false;
	}

	//TU JE BIO PROBLEM, Nismo stavili refleksivnost za niz(int) i niz(char)
	//True ako je A moguce implicitno castati na B
	static boolean castable(String A, String B) {
		switch (A) {
			case "int":
				if(B.equals("const(int)"))
					return true;
				if(B.equals("int")) {
					return true;
				}
				break;
			case "char":
				if (B.equals("int"))
					return true;
				if (B.equals("const(int)"))
					return true;
				if (B.equals("const(char)"))
					return true;
				if(B.equals("char"))
					return true;
				break;
			case "const(int)":
				if (B.equals("int"))
					return true;
				if(B.equals("const(int)"))
					return true;
				break;
			case "const(char)":
				if (B.equals("char"))
					return true;
				if (B.equals("int"))
					return true;
				if (B.equals("const(int)"))
					return true;
				if(B.equals("const(char)"))
					return true;
				break;
			case "niz(char)":
				if(B.equals("niz(const(char))"))
					return true;
				if(B.equals("niz(char)"))
					return true;
				break;
			case "niz(int)":
				if(B.equals("niz(const(int))"))
					return true;
				if(B.equals("niz(int)"))
					return true;
				break;
			default:
				System.err.println(A + " ~ " + B + " :: Ne postoji tip");
		}
		System.err.println(A + " ~ " + B + " :: Nije moguce castati");
		return false;
	}

	static boolean definicija_funkcije(Node aktivni) {
		boolean error = false;
		String tipFunkcije;
		Node n = aktivni.djeca.getFirst();

		// <definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA KR_VOID D_ZAGRADA
		// <slozena_naredba>
		if (aktivni.djeca.get(3).name.contains("KR_VOID")) {
			String printString = "<definicija_funkcije> ::= <ime_tipa> " + pZ(aktivni.djeca.get(1)) + " "
					+ pZ(aktivni.djeca.get(2)) + " " + pZ(aktivni.djeca.get(3)) + " " + pZ(aktivni.djeca.get(4))
					+ " <slozena_naredba>";
			error = ime_tipa(n);
			if (error) {
				return true;
			}
			tipFunkcije = n.podaciNodea.tip;

			if (n.podaciNodea.tip.equals("const(int)") || n.podaciNodea.tip.equals("const(char)")) {
				System.out.println(printString);
				return true;
			}

			n = aktivni.djeca.get(1);
			String funName = n.name.split(" ")[2];

			if (globalna.deklariraneVar.containsKey(funName)) {
				String[] defDek = globalna.deklariraneVar.get(funName).split("%%");
				if (defDek[0].equals("DEF")) {
					System.out.println(printString);
					return true;
				} else {
					if (defDek[1].equals("void->" + tipFunkcije)) {
						globalna.deklariraneVar.put(funName, "DEF%%void->" + tipFunkcije);
					} else {
						System.out.println(printString);
						return true;
					}
				}
			} else {
				globalna.deklariraneVar.put(funName, "DEF%%void->" + tipFunkcije);
			}

			currFunc = aktivni.djeca.get(1).name.split(" ")[2];

			error = slozena_naredba(aktivni.djeca.getLast(), false);
			if (error) {
				return true;
			}

			currFunc = "##none##";

			// <definicija_funkcije> ::= <ime_tipa> IDN L_ZAGRADA <lista_parametara>
			// D_ZAGRADA <slozena_naredba>
		} else {
			String printString = "<definicija_funkcije> ::= <ime_tipa> " + pZ(aktivni.djeca.get(1)) + " "
					+ pZ(aktivni.djeca.get(2)) + " <lista_parametara> " + pZ(aktivni.djeca.get(4))
					+ " <slozena_naredba>";
			//1
			error = ime_tipa(n);
			if (error) {
				return true;
			}
			tipFunkcije = n.podaciNodea.tip;

			//2
			if (n.podaciNodea.tip.equals("const(int)") || n.podaciNodea.tip.equals("const(char)")) {
				System.out.println(printString);
				return true;
			}

			n = aktivni.djeca.get(1);
			String funName = n.name.split(" ")[2];

			//3
			if (globalna.deklariraneVar.containsKey(funName)) {
				String[] defDek = globalna.deklariraneVar.get(funName).split("%%");
				//Ako je redefinicija
				if (defDek[0].equals("DEF")) {
					System.out.println(printString);
					return true;
				//Nije redefincija
				} else {
					n = aktivni.djeca.get(3);
					error = lista_parametara(n);
					if (error) {
						return true;
					}

					//Priprema Stringa
					StringBuilder sb = new StringBuilder();
					for(int i = 0; i < n.podaciNodea.tipovi.size(); i++) {
						if(i == n.podaciNodea.tipovi.size() - 1) {
							sb.append(n.podaciNodea.tipovi.get(i));
						}else {
							sb.append(n.podaciNodea.tipovi.get(i) + ",");
						}
					}

					//Ako su istog tipa sve ok
					if (defDek[1].equals(sb.toString() + "->" + tipFunkcije)) {
						globalna.deklariraneVar.put(funName, "DEF%%" + sb.toString() + "->" + tipFunkcije);
					} else {
						System.out.println(printString);
						return true;
					}
				}
			} else {
				n = aktivni.djeca.get(3);
				error = lista_parametara(n);
				if (error) {
					return true;
				}
				StringBuilder sb = new StringBuilder();
				for(int i = 0; i < n.podaciNodea.tipovi.size(); i++) {
					if(i == n.podaciNodea.tipovi.size() - 1) {
						sb.append(n.podaciNodea.tipovi.get(i));
					}else {
						sb.append(n.podaciNodea.tipovi.get(i) + ",");
					}
				}
				//TU JE BIO PROBLEM u hardcodiranom void umjesto da je lista tipova arg funkcije
				globalna.deklariraneVar.put(funName, "DEF%%" + sb.toString() + "->" + tipFunkcije);
			}


			TablicaZnakova nova = new TablicaZnakova(trenutna);
			sveTablice.add(nova);
			trenutna = nova;

			// inicijalizacija nove tablice za slozenu naredbu
			n = aktivni.djeca.get(3);
			for (int i = 0; i < n.podaciNodea.imena.size(); i++) {
				trenutna.deklariraneVar.put(n.podaciNodea.imena.get(i), n.podaciNodea.tipovi.get(i));
			}

			currFunc = aktivni.djeca.get(1).name.split(" ")[2];

			error = slozena_naredba(aktivni.djeca.getLast(), true);
			if (error) {
				return true;
			}

			currFunc = "##none##";
		}

		return error;
	}



	static boolean prijevodna_jedinica(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <prijevodna_jedinica> -> <prijevodna_jedinica> <vanjska_deklaracija>
		if (n.name.equals("<prijevodna_jedinica>")) {
			error = prijevodna_jedinica(aktivni.djeca.getFirst());
			if (error) {
				return true;
			}
				
			error = vanjska_deklaracija(aktivni.djeca.getLast());
			if (error) {
				return true;
			}

			// <prijevodna_jedinica> -> <vanjska_deklaracija>
		} else {
			error = vanjska_deklaracija(aktivni.djeca.getFirst());
			if (error) {
				return true;
			}
		}

		return error;
	}

	static boolean vanjska_deklaracija(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <vanjska_deklaracija> -> <definicija_funkcije>
		if (n.name.equals("<definicija_funkcije>")) {
			error = definicija_funkcije(aktivni.djeca.getFirst());
			if (error) {
				return true;
			}

			// <vanjska_deklaracija> -> <deklaracija>
		} else {
			error = deklaracija(aktivni.djeca.getFirst());
			if (error) {
				return true;
			}
		}

		return error;
	}

	static boolean izraz_pridruzivanja(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <izraz_pridruzivanja> -> <log_ili_izraz>
		if (n.name.equals("<log_ili_izraz>")) {
			error = log_ili_izraz(n);

			if (error) {
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.lIzraz = n.podaciNodea.lIzraz;

			// <izraz_pridruzivanja> -> <postfiks_izraz> OP_PRIDRUZI <izraz_pridruzivanja>
		} else {
			String printFunction = "<izraz_pridruzivanja> ::= <postfiks_izraz> " + pZ(aktivni.djeca.get(1)) + " <izraz_pridruzivanja>";

			error = postfiks_izraz(n);
			if (error) {
				return true;
			}

			error = (n.podaciNodea.lIzraz == 0);
			if (error) {
				System.out.println(printFunction);
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.lIzraz = 0;


			n = aktivni.djeca.getLast();
			error = izraz_pridruzivanja(n);
			if (error) {
				return true;
			}

			error = !castable(aktivni.djeca.getFirst().podaciNodea.tip, n.podaciNodea.tip);
			if (error) {
				System.out.println(printFunction);
				return true;
			}
		}

		return error;
	}

	static boolean log_i_izraz(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <log_i_izraz> -> <bin_ili_izraz>
		if (n.name.equals("<bin_ili_izraz>")) {
			error = bin_ili_izraz(n);
			if (error) {
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.lIzraz = n.podaciNodea.lIzraz;

			// <log_i_izraz> -> <log_i_izraz> OP_I <bin_ili_izraz>
		} else {

			String printFunction = "<log_i_izraz> " + pZ(aktivni.djeca.get(1)) + " <bin_ili_izraz>";

			aktivni.podaciNodea.tip = "int";
			aktivni.podaciNodea.lIzraz = 0;

			error = log_i_izraz(n);
			if (error) {
				return true;
			}
				
			error = !castable(n.podaciNodea.tip, "int");
			if (error) {
				System.out.println(printFunction);
				return true;
			}

			n = aktivni.djeca.getLast();
			error = bin_ili_izraz(n);
			if (error) {
				return true;
			}
			error = !castable(n.podaciNodea.tip, "int");
			if (error) {
				System.out.println(printFunction);
				return true;
			}
		}

		return error;
	}

	static boolean log_ili_izraz(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <log_ili_izraz> -> <log_i_izraz>
		if (n.name.equals("<log_i_izraz>")) {
			error = log_i_izraz(n);
			if (error) {
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.lIzraz = n.podaciNodea.lIzraz;

			// <log_ili_izraz> -> <log_ili_izraz> OP_ILI <log_i_izraz>
		} else {

			String printFunction = "<log_ili_izraz> " + pZ(aktivni.djeca.get(1)) + " <log_i_izraz>";

			aktivni.podaciNodea.tip = "int";
			aktivni.podaciNodea.lIzraz = 0;

			error = log_ili_izraz(n);
			if (error) {
				return true;
			}

			error = !castable(n.podaciNodea.tip, "int");
			if (error) {
				System.out.println(printFunction);
				return true;
			}

			n = aktivni.djeca.getLast();
			error = log_i_izraz(n);
			if (error) {
				return true;
			}

			error = !castable(n.podaciNodea.tip, "int");
			if (error) {
				System.out.println(printFunction);
				return true;
			}
		}

		return error;
	}

	static boolean izraz(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <izraz> -> <izraz_pridruzivanja>
		if (n.name.equals("<izraz_pridruzivanja>")) {
			error = izraz_pridruzivanja(n);
			if (error) {
				return true;
			}
				

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.lIzraz = n.podaciNodea.lIzraz;

			// <izraz> -> <izraz> ZAREZ <izraz_pridruzivanja>
		} else {
			error = izraz(n);
			if (error) {
				return true;
			}

			n = aktivni.djeca.getLast();
			error = izraz_pridruzivanja(n);
			if (error) {
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.lIzraz = 0;
		}

		return error;
	}

	static boolean primarni_izraz(Node aktivni) {
		Error error = new Error();

		Node prvoDijete = aktivni.djeca.getFirst();
		String[] imePrvoDijete = prvoDijete.name.split(" ");

		// <primarni_izraz> ::= IDN
		if (imePrvoDijete[0].startsWith("IDN")) {
			if(!provjeriTablicu(trenutna, prvoDijete)) {
				error.set();
				System.out.println("<primarni_izraz> ::= " + pZ(prvoDijete));
			}else{
				aktivni.podaciNodea.tip = prvoDijete.podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = prvoDijete.podaciNodea.lIzraz;
			}
			// <primarni_izraz> ::= BROJ
		} else if (imePrvoDijete[0].startsWith("BROJ")) {
			//1
			try {
				String parseString = imePrvoDijete[2];

				if(!unarniOperatori.isEmpty() && (unarniOperatori.get(unarniOperatori.size() - 1).equals("-")
					|| unarniOperatori.get(unarniOperatori.size() - 1).equals("+"))) {
					String unarniString = unarniOperatori.get(unarniOperatori.size() - 1);
					unarniOperatori.remove(unarniOperatori.size() - 1);
					parseString = unarniString + parseString;
				}

				Integer.parseInt(parseString);
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			} catch (Exception e) {
				error.set();
				System.out.println("<primarni_izraz> ::= " + pZ(prvoDijete));
			}

			// <primarni_izraz> ::= ZNAK
		} else if (imePrvoDijete[0].startsWith("ZNAK")) {
			
			if(!ispravanNiz(imePrvoDijete[2])) {
				error.set();
				System.out.println("<primarni_izraz> ::= " + pZ(prvoDijete));
			}else {
				aktivni.podaciNodea.tip = "char";
				aktivni.podaciNodea.lIzraz = 0;
			}

			// <primarni_izraz> ::= NIZ_ZNAKOVA
		} else if (imePrvoDijete[0].startsWith("NIZ_ZNAKOVA")) {

			if(!ispravanNiz(imePrvoDijete[2])) {
				error.set();
				System.out.println("<primarni_izraz> ::= " + pZ(prvoDijete));
			}else {
				aktivni.podaciNodea.tip = "niz(const(char))";
				aktivni.podaciNodea.lIzraz = 0;
			}

			// <primarni_izraz> ::= L_ZAGRADA <izraz> D_ZAGRADA
		} else {
			if(izraz(aktivni.djeca.get(1))) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.get(1).podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.get(1).podaciNodea.lIzraz;
			}

		}

		return error.get();
	}

	static boolean postfiks_izraz(Node aktivni) {
		Error error = new Error();
		int length = aktivni.djeca.size();

		// <postfiks_izraz> ::= <primarni_izraz>
		if (aktivni.djeca.getFirst().name.equals("<primarni_izraz>")) {
			if (primarni_izraz(aktivni.djeca.getFirst())) { 
				return true;
			} else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}

			// <postfiks_izraz> ::= <postfiks_izraz> L_UGL_ZAGRADA <izraz> D_UGL_ZAGRADA
		} else if (length > 3 && aktivni.djeca.get(2).name.equals("<izraz>")) {
			// 1
			if (postfiks_izraz(aktivni.djeca.getFirst())) return true;
			else {
				// 
				//System.out.println(aktivni.podaciNodea.tip);
				String postTip = aktivni.djeca.getFirst().podaciNodea.tip;
				if (!(postTip.equals("niz(int)") || postTip.equals("niz(char)") || postTip.equals("niz(const(int))")
						|| postTip.equals("niz(const(char))"))) error.set();
				else {
					// 3
					if (izraz(aktivni.djeca.get(2))) return true;
					else {
						// 4
						if (!castable(aktivni.djeca.get(2).podaciNodea.tip, "int"))
							error.set();
					}
				}
			}
			if (error.get()) {
				System.out.println("<postfiks_izraz> ::= <postfiks_izraz> " + pZ(aktivni.djeca.get(1)) + " <izraz> "
						+ pZ(aktivni.djeca.getLast()));
			} else {
				// npr. niz(int) ili niz(const(char))
				String postTip = aktivni.djeca.getFirst().podaciNodea.tip;
				int indexPrvaZ = postTip.indexOf("(");
				int indexZadnjaZ = postTip.lastIndexOf(")");
				String temp = postTip.substring(indexPrvaZ + 1, indexZadnjaZ);
				aktivni.podaciNodea.tip = temp;
				if (temp.startsWith("const")) {
					aktivni.podaciNodea.lIzraz = 0;
				} else {
					aktivni.podaciNodea.lIzraz = 1;
				}
			}

			// <postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA D_ZAGRADA
		} else if (length > 2 && aktivni.djeca.get(2).name.startsWith("D_ZAGRADA")) {

			// 1
			if (postfiks_izraz(aktivni.djeca.getFirst())) return true;
			else {
				String postTip = aktivni.djeca.getFirst().podaciNodea.tip;
				// 2
				if (!postTip.startsWith("funkcija(void->")) error.set();

				if (error.get()) {
					System.out.println("<postfiks_izraz> ::= <postfiks_izraz> " 
					+ pZ(aktivni.djeca.get(1)) + " " + pZ(aktivni.djeca.getLast()));
				}else {
					aktivni.podaciNodea.tip = postTip.substring(postTip.indexOf(">") + 1, postTip.lastIndexOf(")"));
					aktivni.podaciNodea.lIzraz = 0;
				}
			}
		
		//<postfiks_izraz> ::= <postfiks_izraz> L_ZAGRADA <lista_argumenata> D_ZAGRADA
		}else if(length > 3 && aktivni.djeca.get(2).name.equals("<lista_argumenata>")) {
			String pomocniZaTip = null;
			//1
			if(postfiks_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(lista_argumenata(aktivni.djeca.get(2))) return true;
				else {
					String postTip = aktivni.djeca.getFirst().podaciNodea.tip;
					ArrayList<String> arg_tip = aktivni.djeca.get(2).podaciNodea.tipovi;
					if(postTip.startsWith("funkcija(")) {
						String[] pTipPolje = postTip.split("->");
						pomocniZaTip = pTipPolje[1].substring(0, pTipPolje[1].indexOf(")"));
						if(pTipPolje.length > 1) {
							String params = pTipPolje[0].substring(pTipPolje[0].indexOf("(") + 1);
							String[] param_tip = params.split(",");
							if(arg_tip.size() != param_tip.length) {
								error.set();
							}else {
								for(int i = 0; i < arg_tip.size(); i++) {
									/*TU JE BIO PROBLEM sto ima samo jedan param_tip pa indeks
									izade van granica param_tip, drugi problem je ako je manje 
									arg_tip od param_tip onda se gleda castable samo za neke 
									a ne za sve i ne nastane greska (recimo ako se preda premalo
									argumenata funkciji)!*/
									if(!castable(arg_tip.get(i), param_tip[i])) {
										error.set();
										break;
									}
								}
							}
						}else error.set();
					}else error.set();
				}

				if(error.get()) {
					System.out.println("<postfiks_izraz> ::= <postfiks_izraz> " + pZ(aktivni.djeca.get(1))
					 + " <lista_argumenata> " + pZ(aktivni.djeca.get(3)));
				}else {
					aktivni.podaciNodea.tip = pomocniZaTip;
					aktivni.podaciNodea.lIzraz = 0;
				}
			
			}
		//<postfiks_izraz> ::= <postfiks_izraz> (OP_INC | OP_DEC)
		}else {
			//1
			if(postfiks_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(aktivni.djeca.getFirst().podaciNodea.lIzraz == 1 && castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) {
					aktivni.podaciNodea.tip = "int";
					aktivni.podaciNodea.lIzraz = 0;
				}else {
					System.out.println("<postfiks_izraz> ::= <postfiks_izraz> " + pZ(aktivni.djeca.getLast()));
					error.set();
				}
			}
		}

		return error.get();
	}

	static boolean lista_argumenata(Node aktivni) {
		Error error = new Error();

		//<lista_argumenata> ::= <izraz_pridruzivanja>
		if(aktivni.djeca.getFirst().name.equals("<izraz_pridruzivanja>")) {
			//1
			if(izraz_pridruzivanja(aktivni.djeca.getFirst())) {
				return true;
			}
			else {
				aktivni.podaciNodea.tipovi.add(aktivni.djeca.getFirst().podaciNodea.tip);
			}

		//<lista_argumenata> ::= <lista_argumenata> ZAREZ <izraz_pridruzivanja>
		}else {
			//1
			if(lista_argumenata(aktivni.djeca.getFirst())) {
				return true;
			}else {
				//2
				if(izraz_pridruzivanja(aktivni.djeca.getLast())) return true;
			}

			if(error.get()) {
				System.out.println("<lista_argumenata> ::= <lista_argumenata> " + pZ(aktivni.djeca.get(1)) + " <izraz_pridruzivanja>");
			}else {
				aktivni.podaciNodea.tipovi.addAll(aktivni.djeca.getFirst().podaciNodea.tipovi);
				aktivni.podaciNodea.tipovi.add(aktivni.djeca.getLast().podaciNodea.tip);
			}
		}

		return error.get();
	}

	static boolean unarni_izraz(Node aktivni) {
		Error error = new Error();

		String skretnica = aktivni.djeca.getFirst().name;

		//<unarni_izraz> ::= <postfiks_izraz>
		if(skretnica.equals("<postfiks_izraz>")) {
			//1
			if(postfiks_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}

		//<unarni_izraz> ::= <unarni_operator> <cast_izraz>
		}else if(skretnica.startsWith("<unarni_operator>")) {

			//zabiljezi velicinu unarnih operatora na pocetku
			int lengthUn = unarniOperatori.size();

			//zabiljeziti unarni operator
			unarniOperatori.add(aktivni.djeca.getFirst().djeca.getFirst().name.split(" ")[2]);

			//1
			if(cast_izraz(aktivni.djeca.getLast())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
			}

			//ako nije iskoristen unarni operator, ukloni ga sam
			while(lengthUn < unarniOperatori.size()) {
				unarniOperatori.remove(unarniOperatori.size() - 1);
			}

			if(error.get()) {
				System.out.println("<unarni_izraz> ::= <unarni_operator> <cast_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}

		//<unarni_izraz> ::= (OP_INC | OP_DEC) <unarni_izraz>
		}else {
			//1
			if(unarni_izraz(aktivni.djeca.getLast())) return true;
			else {
				//2
				if(aktivni.djeca.getLast().podaciNodea.lIzraz == 1 && castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) {
					aktivni.podaciNodea.tip = "int";
					aktivni.podaciNodea.lIzraz = 0;
				}else {
					error.set();
				}
			}

			if(error.get()) {
				System.out.println("<unarni_izraz> ::= " + pZ(aktivni.djeca.getFirst()) + " <unarni_izraz>");
			}
		}

		return error.get();
	}

	static boolean cast_izraz(Node aktivni) {
		Error error = new Error();

		//<cast_izraz> ::= <unarni_izraz>
		if(aktivni.djeca.getFirst().name.equals("<unarni_izraz>")) {
			//1
			if(unarni_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}

		//<cast_izraz> ::= L_ZAGRADA <ime_tipa> D_ZAGRADA <cast_izraz>
		}else {
			//1
			if(ime_tipa(aktivni.djeca.get(1))) return true;
			else {
				if(cast_izraz(aktivni.djeca.getLast())) return true;
				else {
					if(!expCastable(aktivni.djeca.getLast().podaciNodea.tip, aktivni.djeca.get(1).podaciNodea.tip)) error.set();
				}
			}

			if(error.get()) {
				System.out.println("<cast_izraz> ::= " + pZ(aktivni.djeca.getFirst()) + " <ime_tipa> "
				 + pZ(aktivni.djeca.get(2)) + " <cast_izraz>");
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.get(1).podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = 0;
			}
		}

		return error.get();
	}

	static boolean ime_tipa(Node aktivni) {
		Error error = new Error();

		//<ime_tipa> ::= <specifikator_tipa>
		if(aktivni.djeca.getFirst().name.equals("<specifikator_tipa>")) {
			//1
			if(specifikator_tipa(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
			}

		//<ime_tipa> ::= KR_CONST <specifikator_tipa>
		}else {
			//1
			if(specifikator_tipa(aktivni.djeca.getLast())) return true;
			else {
				if(!aktivni.djeca.getLast().podaciNodea.tip.equals("void")) {
					aktivni.podaciNodea.tip = "const(" + aktivni.djeca.getLast().podaciNodea.tip + ")";
				}else error.set();
			}

			if(error.get()){ 
				System.out.println("<ime_tipa> ::= " + pZ(aktivni.djeca.getFirst()) + " <specifikator_tipa>");
			}

		}

		return error.get();
	}

	static boolean specifikator_tipa(Node aktivni) {
		Error error = new Error();

		//<specifikator_tipa> ::= KR_VOID
		if(aktivni.djeca.getFirst().name.startsWith("KR_VOID")) {
			aktivni.podaciNodea.tip = "void";

		//<specifikator_tipa> ::= KR_CHAR
		}else if(aktivni.djeca.getFirst().name.startsWith("KR_CHAR")) {
			aktivni.podaciNodea.tip = "char";

		//<specifikator_tipa> ::= KR_INT
		}else {
			aktivni.podaciNodea.tip = "int";
		}

		return error.get();
	}

	static boolean multiplikativni_izraz(Node aktivni) {
		Error error = new Error();

		//<multiplikativni_izraz> ::= <cast_izraz>
		if(aktivni.djeca.getFirst().name.equals("<cast_izraz>")) {
			//1
			if(cast_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}

		//<multiplikativni_izraz> ::= <multiplikativni_izraz> (OP_PUTA |OP_DIJELI | OP_MOD) <cast_izraz>
		}else {
			//1
			if(multiplikativni_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(cast_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<multiplikativni_izraz> ::= <multiplikativni_izraz> "
				 + pZ(aktivni.djeca.get(1)) + " <cast_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}
		}
		
		return error.get();
	}

	static boolean aditivni_izraz(Node aktivni)  {
		Error error = new Error();

		//<aditivni_izraz> ::= <multiplikativni_izraz>
		if(aktivni.djeca.getFirst().name.equals("<multiplikativni_izraz>")) {
			//1
			if(multiplikativni_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
					aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
					aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}
		//<aditivni_izraz> ::= <aditivni_izraz> (PLUS | MINUS) <multiplikativni_izraz>
		}else {
			//1
			if(aditivni_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(multiplikativni_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<aditivni_izraz> ::= <aditivni_izraz> " + pZ(aktivni.djeca.get(1)) + " <multiplikativni_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;	
			}
		}

		return error.get();
	}

	static boolean odnosni_izraz(Node aktivni) {
		Error error = new Error();

		//<odnosni_izraz> ::= <aditivni_izraz>
		if(aktivni.djeca.getFirst().name.equals("<aditivni_izraz>")) {
			if(aditivni_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}
		//<odnosni_izraz> ::= <odnosni_izraz> (OP_LT | OP_GT | OP_LTE | OP_GTE) <aditivni_izraz>
		}else {
			//1
			if(odnosni_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(aditivni_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<odnosni_izraz> ::= <odnosni_izraz> " + pZ(aktivni.djeca.get(1)) + " <aditivni_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}
		}

		return error.get();
	}

	static boolean jednakosni_izraz(Node aktivni) {
		Error error = new Error();

		//<jednakosni_izraz> ::= <odnosni_izraz>
		if(aktivni.djeca.getFirst().name.equals("<odnosni_izraz>")) {
			if(odnosni_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}
		//<jednakosni_izraz> ::= <jednakosni_izraz> (OP_EQ | OP_NEQ) <odnosni_izraz>
		}else {
			//1
			if(jednakosni_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(odnosni_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<jednakosni_izraz> ::= <jednakosni_izraz> " + pZ(aktivni.djeca.get(1)) + " <odnosni_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}
		}

		return error.get();
	}

	static boolean bin_i_izraz(Node aktivni) {
		Error error = new Error();

		//<bin_i_izraz> ::= <jednakosni_izraz>
		if(aktivni.djeca.getFirst().name.equals("<jednakosni_izraz>")) {
			if(jednakosni_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}
		//<bin_i_izraz> ::= <bin_i_izraz> OP_BIN_I <jednakosni_izraz>
		}else {
			//1
			if(bin_i_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(jednakosni_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<bin_i_izraz> ::= <bin_i_izraz> " + pZ(aktivni.djeca.get(1)) + " <jednakosni_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}
		}

		return error.get();
	}

	static boolean bin_xili_izraz(Node aktivni) {
		Error error = new Error();

		//<bin_xili_izraz> ::= <bin_i_izraz>
		if(aktivni.djeca.getFirst().name.equals("<bin_i_izraz>")) {
			if(bin_i_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}
		//<bin_xili_izraz> ::= <bin_xili_izraz> OP_BIN_XILI <bin_i_izraz>
		}else {
			//1
			if(bin_xili_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(bin_i_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<bin_xili_izraz> ::= <bin_xili_izraz> " + pZ(aktivni.djeca.get(1)) + " <bin_i_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}
		}

		return error.get();
	}

	static boolean bin_ili_izraz(Node aktivni) {
		Error error = new Error();

		//<bin_ili_izraz> ::= <bin_xili_izraz>
		if(aktivni.djeca.getFirst().name.equals("<bin_xili_izraz>")) {
			if(bin_xili_izraz(aktivni.djeca.getFirst())) {
				return true;
			}else {
				aktivni.podaciNodea.tip = aktivni.djeca.getFirst().podaciNodea.tip;
				aktivni.podaciNodea.lIzraz = aktivni.djeca.getFirst().podaciNodea.lIzraz;
			}
		//<bin_ili_izraz> ::= <bin_ili_izraz> OP_BIN_ILI <bin_xili_izraz>
		}else {
			//1
			if(bin_ili_izraz(aktivni.djeca.getFirst())) return true;
			else {
				//2
				if(!castable(aktivni.djeca.getFirst().podaciNodea.tip, "int")) error.set();
				else {
					//3
					if(bin_xili_izraz(aktivni.djeca.getLast())) return true;
					else {
						//4
						if(!castable(aktivni.djeca.getLast().podaciNodea.tip, "int")) error.set();
					}
				}
			}

			if(error.get()) {
				System.out.println("<bin_ili_izraz> ::= <bin_ili_izraz> " + pZ(aktivni.djeca.get(1)) + " <bin_xili_izraz>");
			}else {
				aktivni.podaciNodea.tip = "int";
				aktivni.podaciNodea.lIzraz = 0;
			}
		}

		return error.get();
	}

	static boolean slozena_naredba(Node aktivni, boolean init) {
		int length = aktivni.djeca.size();
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		//init varijabla daje mogucnost inicijalizacije nove tablice u drugoj funkciji npr. definicija_funkcije()
		if(!init) {
			TablicaZnakova nova = new TablicaZnakova(trenutna);
			sveTablice.add(nova);
			trenutna = nova;
		}

		// <slozena_naredba> -> L_VIT_ZAGRADA <lista_naredbi> D_VIT_ZAGRADA
		// Treba ici na novu razinu tablice znakova
		if (length <= 3) {
			n = aktivni.djeca.get(1);
			error = lista_naredbi(n);
			if (error) {
				return true;
			}

			// <slozena_naredba> -> L_VIT_ZAGRADA <lista_deklaracija> <lista_naredbi>
			// D_VIT_ZAGRADA
			// Treba ici na novu razinu tablice znakova
		} else {
			n = aktivni.djeca.get(1);
			error = lista_deklaracija(n);
			if (error) {
				return true;
			}

			n = aktivni.djeca.get(2);
			error = lista_naredbi(n);
			if (error) {
				return true;
			}

		}

		//izlazak iz bloka i vracanje roditelja kao trenutnu tablicu
		trenutna = trenutna.roditelj;

		return error;
	}

	static boolean lista_naredbi(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <lista_naredbi> -> <naredba>
		if (n.name.equals("<naredba>")) {
			error = naredba(n);
			if (error) {
				return true;
			}
			// <lista_naredbi> -> <lista_naredbi> <naredba>
		} else {
			error = lista_naredbi(n);
			if (error) {
				return true;
			}

			n = aktivni.djeca.getLast();
			error = naredba(n);
			if (error) {
				return true;
			}
		}

		return error;
	}

	static boolean naredba(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		switch (n.name) {
			case "<slozena_naredba>":
				if (slozena_naredba(n, false)) {
					return true;
				}
				break;
			case "<izraz_naredba>":
				if (izraz_naredba(n)) {
					return true;
				}
				break;
			case "<naredba_grananja>":
				if (naredba_grananja(n)) {
					return true;
				}
				break;
			case "<naredba_petlje>":
				unutarPetlje++;
				if (naredba_petlje(n)) {
					return true;
				}	
				unutarPetlje--;
				break;
			case "<naredba_skoka>":
				if (naredba_skoka(n)) {
					return true;
				}
				break;
			default:
				System.err.println("Nije prepoznata naredba..");
		}
		return false;
	}

	static boolean izraz_naredba(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		// <izraz_naredba> ::= <izraz> TOCKAZAREZ
		if (n.name.equals("<izraz>")) {
			error = izraz(n);
			if (error) {
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
		// <izraz_naredba> -> TOCKAZAREZ
		} else {
			aktivni.podaciNodea.tip = "int";
		}

		return error;
	}

	static boolean naredba_grananja(Node aktivni) {
		int length = aktivni.djeca.size();
		Node n = aktivni.djeca.getFirst();
		boolean error = false;
		String printFunction = "";

		if(length > 5) {
			printFunction = "<naredba_grananja> ::= " + pZ(aktivni.djeca.getFirst())  + " " + pZ(aktivni.djeca.get(1)) + " <izraz> "
							 + pZ(aktivni.djeca.get(3)) + " <naredba> "+ pZ(aktivni.djeca.get(5)) + " <naredba>";
		} else {
			printFunction = "<naredba_grananja> ::= " + pZ(aktivni.djeca.getFirst())  + " " + pZ(aktivni.djeca.get(1)) + " <izraz> " 
							+ pZ(aktivni.djeca.get(3)) + " <naredba> ";
		}

		// <naredba_grananja> -> KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba>
		n = aktivni.djeca.get(2);
		error = izraz(n);
		if (error) {
			return true;
		}
			

		error = !castable(n.podaciNodea.tip, "int");
		if (error) {
			System.out.println(printFunction);
			return true;
		}

		n = aktivni.djeca.get(4);
		error = naredba(n);
		if (error) {
			return true;
		}

		// <naredba_grananja> -> KR_IF L_ZAGRADA <izraz> D_ZAGRADA <naredba>1 KR_ELSE
		// <naredba>2
		if (length > 5) {
			n = aktivni.djeca.getLast();
			error = naredba(n);
			if (error) {
				return true;
			}
		}

		return error;
	}

	static boolean naredba_petlje(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		int length = aktivni.djeca.size();
		boolean error = false;

		// <naredba_petlje> -> KR_WHILE L_ZAGRADA <izraz> D_ZAGRADA <naredba>
		if (length <= 5) {
			String printFunction = "<naredba_petlje> ::= " + pZ(aktivni.djeca.getFirst()) + " " 
									+  pZ(aktivni.djeca.get(1)) + " <izraz> " + pZ(aktivni.djeca.get(3)) + " <naredba>";

			n = aktivni.djeca.get(2);
			error = izraz(n);
			if (error) {
				return true;
			}
				

			error = !castable(n.podaciNodea.tip, "int");
			if (error) {
				System.out.println(printFunction);
				return true;
			}

			n = aktivni.djeca.getLast();
			error = naredba(n);
			if (error) {
				return true;
			}

			// <naredba_petlje> ::= KR_FOR L_ZAGRADA <izraz_naredba>1 <izraz_naredba>2
			// D_ZAGRADA <naredba>
			// || KR_FOR L_ZAGRADA <izraz_naredba>1 <izraz_naredba>2 <izraz> D_ZAGRADA
			// <naredba>
		} else {
			String printFunction = "";
			if(length > 6) {
				printFunction = "<naredba_petlje> ::= " + pZ(aktivni.djeca.getFirst()) + " "+ pZ(aktivni.djeca.get(1))
							    + " <izraz_naredba> <izraz_naredba> <izraz> "+ pZ(aktivni.djeca.get(5)) + " <naredba>";
			} else {
				printFunction = "<naredba_petlje> ::= " + pZ(aktivni.djeca.getFirst()) + " "+ pZ(aktivni.djeca.get(1))
				+ " <izraz_naredba> <izraz_naredba> "+ pZ(aktivni.djeca.get(4)) + " <naredba>";
			}

			n = aktivni.djeca.get(2);
			error = izraz_naredba(n);
			if (error) {
				return true;
			}

			n = aktivni.djeca.get(3);
			error = izraz_naredba(n);
			if (error) {
				return true;
			}

			error = !castable(n.podaciNodea.tip, "int");
			if (error) {
				System.out.println(printFunction);
				return true;
			}

			if (length > 6) {
				n = aktivni.djeca.get(4);
				error = izraz(n);
				if (error) {
					return true;
				}
			}

			n = aktivni.djeca.get(length - 1);
			error = naredba(n);
			if (error) {
				System.out.println(printFunction);
				return true;
			};
		}

		return error;
	}

	static boolean naredba_skoka(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;
		int length = aktivni.djeca.size();

		TablicaZnakova radna = provjeriTablicu(trenutna, currFunc);
		boolean containsFun;
		if(radna != null) {
			containsFun = true;
		}else containsFun = false;

		// <naredba_skoka> -> KR_RETURN <izraz> TOCKAZAREZ
		if (length == 3) {
			String printFunction = "<naredba_skoka> ::= " + pZ(aktivni.djeca.getFirst()) + " <izraz> " + pZ(aktivni.djeca.getLast());
			n = aktivni.djeca.get(1);
			error = izraz(n);
			if (error) {
				return true;
			}

			if (containsFun) {
				String tipFun = radna.deklariraneVar.get(currFunc).split("->")[1].replace(")", "");
				
				if(!castable(n.podaciNodea.tip, tipFun)) {
					System.out.println(printFunction);
					return true;
				};
			} else {
				System.out.println(printFunction);
				return true;
			}

		// <naredba_skoka> -> KR_CONTINUE TOCKAZAREZ
		} else if (length == 2 && (n.name.contains("KR_CONTINUE"))) {
			if (unutarPetlje == 0) {
				System.out.println("<naredba_skoka> ::= " + pZ(aktivni.djeca.getFirst()) + " " + pZ(aktivni.djeca.getLast()));
				return true;
			}
		// <naredba_skoka> -> KR_BREAK TOCKAZAREZ
		} else if(length == 2 && n.name.contains("KR_BREAK")) {
			if (unutarPetlje == 0) {
				System.out.println("<naredba_skoka> ::= " + pZ(aktivni.djeca.getFirst()) + " " + pZ(aktivni.djeca.getLast()));
				return true;
			}
		// <naredba_skoka> -> KR_RETURN TOCKAZAREZ
		} else {
			if (!(containsFun && radna.deklariraneVar.get(currFunc).split("->")[1].contains("void"))) {
				System.out.println("<naredba_skoka> ::= " + pZ(aktivni.djeca.getFirst()) + " " + pZ(aktivni.djeca.getLast()));
				return true;
			}
		}

		return error;
	}

	static boolean lista_parametara(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		//<lista_parametara> ::= <deklaracija_parametra>
		if(n.name.equals("<deklaracija_parametra>")) {
			error = deklaracija_parametra(n);
			if(error) {
				return true;
			}


			//bitno paziti na poredak tipova i imena!
			aktivni.podaciNodea.tipovi.add(n.podaciNodea.tip);
			aktivni.podaciNodea.imena.add(n.podaciNodea.ime);
		//<lista_parametara> ::= <lista_parametara> ZAREZ <deklaracija_parametra>
		} else {
			error = lista_parametara(n);
			if(error) {
				return true;
			}

			aktivni.podaciNodea.tipovi = n.podaciNodea.tipovi;
			aktivni.podaciNodea.imena = n.podaciNodea.imena;

			n = aktivni.djeca.getLast();
			error = deklaracija_parametra(n);
			if(error) {
				return true;
			}

			if(aktivni.podaciNodea.imena.contains(n.podaciNodea.ime)) {
				System.out.println("<lista_parametara> ::= <lista_parametara> " + pZ(aktivni.djeca.get(1)) + " <deklaracija_parametra>");
				return true;
			}
			
			aktivni.podaciNodea.tipovi.add(n.podaciNodea.tip);
			aktivni.podaciNodea.imena.add(n.podaciNodea.ime);
		}
	  return error;
	}

	static boolean deklaracija_parametra(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;
		int length = aktivni.djeca.size();

		//<deklaracija_parametra> ::= <ime_tipa> IDN
		if(length <= 2) {
			error = ime_tipa(n);
			if(error) {
				return true;
			}

			if(n.podaciNodea.tip.equals("void")) {
				System.out.println("<deklaracija_parametra> ::= <ime_tipa> " + pZ(aktivni.djeca.getLast()));
				return true;
			}

			aktivni.podaciNodea.tip = n.podaciNodea.tip;
			aktivni.podaciNodea.ime = aktivni.djeca.getLast().name.split(" ")[2];
			
		//<deklaracija_parametra> ::= <ime_tipa> IDN L_UGL_ZAGRADA D_UGL_ZAGRADA
		} else {
			error = ime_tipa(n);
			if(error) {
				return true;
			}

			if(n.podaciNodea.tip.equals("void")) {
				System.out.println("<deklaracija_parametra> ::= <ime_tipa> " + pZ(aktivni.djeca.get(1)) + " "
				+ pZ(aktivni.djeca.get(2)) + " " + pZ(aktivni.djeca.get(3)));
				return true;
			}

			aktivni.podaciNodea.tip = "niz(" + n.podaciNodea.tip + ")";
			//TU JE BIO PROBLEM! aktivni.djeca.getLast().name.split(" ")[2]; a last je D_UGL_ZAGRADA
			aktivni.podaciNodea.ime = aktivni.djeca.get(1).name.split(" ")[2];
		}
	  return error;
	}

	static boolean lista_deklaracija(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		//<lista_deklaracija> ::= <deklaracija>
		if(n.name.equals("<deklaracija>")) {
			error = deklaracija(n);
			if(error) {
				return true;
			}

		//<lista_deklaracija> ::= <lista_deklaracija> <deklaracija>
		} else {
			error = lista_deklaracija(n);
			if(error) {
				return true;
			}

			n = aktivni.djeca.getLast();
			error = deklaracija(n);
			if(error) {
				return true;
			}
		}
		return error;
	}

	//<deklaracija> ::= <ime_tipa> <lista_init_deklaratora> TOCKAZAREZ
	static boolean deklaracija(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		error = ime_tipa(n);
		if(error) {
			return true;
		}

		n = aktivni.djeca.get(1);
		error = lista_init_deklaratora(n, aktivni.djeca.getFirst().podaciNodea.tip);
		if(error) {
			return true;
		}
	 return error;
	}

	static boolean lista_init_deklaratora(Node aktivni, String danTip) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;

		//nasljedno svojstvo
		if(!danTip.isEmpty()) {
			aktivni.podaciNodea.ntip = danTip;
		}

		//<lista_init_deklaratora> ::= <init_deklarator>
		if(n.name.equals("<init_deklarator>")) {
			error = init_deklarator(n, aktivni.podaciNodea.ntip);
			if(error) {
				return true;
			}

		//<lista_init_deklaratora>1 ::= <lista_init_deklaratora>2 ZAREZ <init_deklarator>
		} else {
			error = lista_init_deklaratora(n, aktivni.podaciNodea.ntip);
			if(error) {
				return true;
			}

			n = aktivni.djeca.getLast();
			error = init_deklarator(n, aktivni.podaciNodea.ntip);
			if(error) {
				return true;
			}
		}

		return error;
	}

	static boolean init_deklarator(Node aktivni, String danTip) {
		Node n = aktivni.djeca.getLast();
		boolean error = false;

		if(!danTip.isEmpty()) {
			aktivni.podaciNodea.ntip = danTip;
		}

		// <init_deklarator> ::= <izravni_deklarator>
		if(n.name.equals("<izravni_deklarator>")) {
			String printFunction = "<init_deklarator> ::= <izravni_deklarator>";

			error = izravni_deklarator(n, aktivni.podaciNodea.ntip);
			if(error) {
				return true;
			}

			if(n.podaciNodea.tip.startsWith("const(") || n.podaciNodea.tip.startsWith("niz(const(")) {
				System.out.println(printFunction);
				return true;
			}
			
		//<init_deklarator> ::= <izravni_deklarator> OP_PRIDRUZI <inicijalizator>
		}else {
			String printFunction = "<init_deklarator> ::= <izravni_deklarator> " + pZ(aktivni.djeca.get(1)) + " <inicijalizator>";

			error = izravni_deklarator(aktivni.djeca.getFirst(), aktivni.podaciNodea.ntip);
			if(error) {
				return true;
			}

			error = inicijalizator(aktivni.djeca.getLast());
			if(error) {
				return true;
			}

			String izrDeklTip = aktivni.djeca.getFirst().podaciNodea.tip;
			Node izrDekl = aktivni.djeca.getFirst();

			if(izrDeklTip.equals("int") || izrDeklTip.equals("char")
			|| izrDeklTip.equals("const(int)") || izrDeklTip.equals("const(char)")) {
				if(!(castable(n.podaciNodea.tip, izrDeklTip))){
					System.out.println(printFunction);
					return true;
				}
			}else if (izrDeklTip.equals("niz(int)") || izrDeklTip.equals("niz(char)")
			|| izrDeklTip.equals("niz(const(int))") || izrDeklTip.equals("niz(const(char))")) {
				if(Integer.parseInt(n.podaciNodea.brElem) <= Integer.parseInt(izrDekl.podaciNodea.brElem)) {
					for(String u : n.podaciNodea.tipovi) {
						if(!(castable(u, izrDeklTip.substring(izrDeklTip.lastIndexOf("(") + 1, izrDeklTip.indexOf(")"))))) {
							System.out.println(printFunction);
							return true;
						}
					}
				}else {
					System.out.println(printFunction);
					return true;
				}
			}else {
				System.out.println(printFunction);
				return true;
			}

		}
		return error;
	}

	static boolean izravni_deklarator(Node aktivni, String danTip) {
		Node n = aktivni.djeca.getFirst();
		int length = aktivni.djeca.size();
		boolean error = false;

		// <izravni_deklarator> ::= IDN
		if(length == 1) {
			if(danTip.equals("void")) {
				System.out.println("<izravni_deklarator> ::= " + pZ(n));
				return true;
			}

			aktivni.podaciNodea.tip = danTip;

			if(trenutna.deklariraneVar.containsKey(n.name.split(" ")[2])) {
				System.out.println("<izravni_deklarator> ::= " + pZ(n));
				return true;
			}
			
			trenutna.deklariraneVar.put(n.name.split(" ")[2], danTip);

		}
		// <izravni_deklarator> ::= IDN L_UGL_ZAGRADA BROJ D_UGL_ZAGRADA
		else if(length == 4 && aktivni.djeca.get(2).name.contains("BROJ")) {
			String printFunction = "<izravni_deklarator> ::= " + pZ(aktivni.djeca.get(0)) + " " + pZ(aktivni.djeca.get(1)) + " " + 
									pZ(aktivni.djeca.get(2)) + " " + pZ(aktivni.djeca.get(3));

			if(danTip.equals("void")) {
				System.out.println(printFunction);
				return true;
			}

			if(trenutna.deklariraneVar.containsKey(n.name.split(" ")[2])) {
				System.out.println("<izravni_deklarator> ::= " + pZ(n));
				return true;
			}

			Integer vrijednostBroja = -1;

			try {
				vrijednostBroja = Integer.parseInt(aktivni.djeca.get(2).name.split(" ")[2]);
			} catch(NumberFormatException exc) {
				System.err.println("Nije moguce parsirati broj...");
			}

			if(!(vrijednostBroja > 0 && vrijednostBroja <= 1024)) {
				System.out.println(printFunction);
				return true;
			}

			aktivni.podaciNodea.tip = "niz(" + danTip + ")";
			aktivni.podaciNodea.brElem = vrijednostBroja.toString();

			trenutna.deklariraneVar.put(n.name.split(" ")[2], aktivni.podaciNodea.tip);

		// <izravni_deklarator> ::= IDN L_ZAGRADA KR_VOID D_ZAGRADA			
		} else if(length == 4 && aktivni.djeca.get(2).name.contains("KR_VOID")) {

			if(trenutna.deklariraneVar.containsKey(n.name.split(" ")[2])) {
				String tipFunkcije = trenutna.deklariraneVar.get(n.name.split(" ")[2]);
				if(!tipFunkcije.split("%%")[1].equals("void->" + danTip)) {
					System.out.println("<izravni_deklarator> ::= " + pZ(aktivni.djeca.get(0)) + " " + pZ(aktivni.djeca.get(1)) + " " + 
					pZ(aktivni.djeca.get(2)) + " " + pZ(aktivni.djeca.get(3)));
					return true;
				}
			} else {
				trenutna.deklariraneVar.put(n.name.split(" ")[2], "DEK%%void->" + danTip);
			}

			aktivni.podaciNodea.tip = "funkcija(void->" + danTip + ")";
		
		// <izravni_deklarator> ::= IDN L_ZAGRADA <lista_parametara> D_ZAGRADA
		} else if(length == 4 && aktivni.djeca.get(2).name.equals("<lista_parametara>")) {
			Node listNode = aktivni.djeca.get(2);
			error = lista_parametara(listNode);
			if(error) return true;

			StringBuilder sb = new StringBuilder();
			for(int i = 0; i < listNode.podaciNodea.tipovi.size(); i++) {
				if(i == listNode.podaciNodea.tipovi.size() - 1) {
					sb.append(listNode.podaciNodea.tipovi.get(i));
				}else {
					sb.append(listNode.podaciNodea.tipovi.get(i) + ",");
				}
			}

			String params = sb.toString();

			if(trenutna.deklariraneVar.containsKey(n.name.split(" ")[2])) {
				String tipFunkcije = trenutna.deklariraneVar.get(n.name.split(" ")[2]);
				if(!tipFunkcije.split("%%")[1].equals(params + "->" + danTip)) {
					System.out.println("<izravni_deklarator> ::= " + pZ(aktivni.djeca.get(0)) + " " + pZ(aktivni.djeca.get(1)) + " <lista_parametara> " + pZ(aktivni.djeca.get(3)));
					return true;
				}
			} else {
				trenutna.deklariraneVar.put(n.name.split(" ")[2], "DEK%%" + params + "->" + danTip);
			}

			aktivni.podaciNodea.tip = "funkcija(" + params + "->" + danTip + ")";
		}
		return error;
	}

	static boolean inicijalizator(Node aktivni) {
		boolean error = false;
		String niz = "";
		Node n = aktivni.djeca.getFirst();

		// <inicijalizator> ::= <izraz_pridruzivanja>
		if(n.name.equals("<izraz_pridruzivanja>")) {
			niz = dfsNiz(n);

			error = izraz_pridruzivanja(n);
			if(error) return true;

			if(!niz.isEmpty()) {
				aktivni.podaciNodea.brElem = Integer.toString(niz.length() + 1);
				for(int i = 0; i < niz.length(); i++) {
					aktivni.podaciNodea.tipovi.add("char");
				}
			} else {
				aktivni.podaciNodea.tip = n.podaciNodea.tip;
			}
		// <inicijalizator> ::= L_VIT_ZAGRADA <lista_izraza_pridruzivanja> D_VIT_ZAGRADA
		} else {
			n = aktivni.djeca.get(1);
			error = lista_izraza_pridruzivanja(n);
			if(error) return true;

			aktivni.podaciNodea.brElem = n.podaciNodea.brElem;
			aktivni.podaciNodea.tipovi = n.podaciNodea.tipovi;
		}

		return error;
	}

	static boolean lista_izraza_pridruzivanja(Node aktivni) {
		Node n = aktivni.djeca.getFirst();
		boolean error = false;
		int length = aktivni.djeca.size();

		// <lista_izraza_pridruzivanja> ::= <izraz_pridruzivanja>
		if(length == 1) {
			error = izraz_pridruzivanja(n);
			if(error) return false;

			aktivni.podaciNodea.tipovi.add(n.podaciNodea.tip);
			aktivni.podaciNodea.brElem = "1";
		// <lista_izraza_pridruzivanja> ::= <lista_izraza_pridruzivanja> ZAREZ <izraz_pridruzivanja>
		} else {
			error = lista_izraza_pridruzivanja(n);
			if(error) return true;

			error = izraz_pridruzivanja(aktivni.djeca.getLast());
			if(error) return true;

			n.podaciNodea.tipovi.add(aktivni.djeca.getLast().podaciNodea.tip);
			aktivni.podaciNodea.tipovi = n.podaciNodea.tipovi;

			aktivni.podaciNodea.brElem = Integer.toString(Integer.parseInt(aktivni.djeca.getFirst().podaciNodea.brElem) + 1);
		}
		return error;
	}

	static String dfsNiz(Node korijen) {
		String valueHold = "";

		if(!korijen.djeca.isEmpty()) {
			for(Node n : korijen.djeca) {
				valueHold = dfsNiz(n);
				if(!valueHold.isEmpty()) {
					return valueHold;
				}
			}
		}

		if(korijen.name.contains("NIZ_ZNAKOVA")) {
			String imasKorijenja = korijen.name.split(" ")[2];
			if(imasKorijenja.startsWith("\"") && imasKorijenja.endsWith("\"")) {
				imasKorijenja = imasKorijenja.substring(1, imasKorijenja.length() - 1);
			}
			return imasKorijenja;
		} else {
			return "";
		}
	}

	static void nakon_obilaska() {
		if(globalna.deklariraneVar.containsKey("main")) {
			String tip = globalna.deklariraneVar.get("main");
			if(!tip.split("%%")[1].equals("void->int")) {
				System.out.println("main");
				return;
			}
		} else {
			System.out.println("main");
			return;
		}
		for(TablicaZnakova radna : sveTablice) {
			for(Map.Entry<String, String> entry : radna.deklariraneVar.entrySet()) {
				String[] tipValue = entry.getValue().split("%%");
				if(tipValue.length > 1) {
					if(tipValue[0].equals("DEK")) {
						System.out.println("funkcija");
						return;
					}
				}
			}
		}
	}

	static void izgradiStablo() {

		HashMap<Integer, Node> temp = new HashMap<Integer, Node>();
		try {
			//ClassLoader.getSystemResourceAsStream(".\\greska2\\test.in")
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			String redak = br.readLine();
			korijen = new Node(redak.trim(), null);
			temp.put(0, korijen);

			// Osiguranje protiv iznimke
			temp.put(-1, null);

			Node radni;

			redak = br.readLine();
			while (redak != null) {
				radni = temp.get(razina(redak) - 1);

				Node tempNode = new Node(redak.trim(), radni);

				radni.djeca.add(tempNode);

				int tempRazina = razina(redak);

				if (temp.containsKey(tempRazina)) {
					temp.replace(tempRazina, tempNode);
				} else {
					temp.put(tempRazina, tempNode);
				}
				redak = br.readLine();
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String args[]) {
		boolean errorDfs = false;
		izgradiStablo();

		// DFS
		errorDfs = prijevodna_jedinica(korijen);

		if(!errorDfs) {
			nakon_obilaska();
		}
	}
}