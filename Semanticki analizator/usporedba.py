import subprocess

print("Skripta za testiranje - 3. labos iz PPJ")
print("22.12.2020.")

#compile SemantickiAnalizator.java
print("Compiling SemantickiAnalizator.java...")
subprocess.run(["javac", "SemantickiAnalizator.java"], shell=True)

print("\n")

print("Primjer 01_idn")
subprocess.run("java SemantickiAnalizator < .\\01_idn\\test.in > .\\01_idn\\mojtest.out", shell=True)
subprocess.run("FC .\\01_idn\\test.out .\\01_idn\\mojtest.out", shell=True)

print("Primjer 03_niz_znakova")
subprocess.run("java SemantickiAnalizator < .\\03_niz_znakova\\test.in > .\\03_niz_znakova\\mojtest.out", shell=True)
subprocess.run("FC .\\03_niz_znakova\\test.out .\\03_niz_znakova\\mojtest.out", shell=True)

print("Primjer 05_impl_int2char")
subprocess.run("java SemantickiAnalizator < .\\05_impl_int2char\\test.in > .\\05_impl_int2char\\mojtest.out", shell=True)
subprocess.run("FC .\\05_impl_int2char\\test.out .\\05_impl_int2char\\mojtest.out", shell=True)

print("Primjer 07_nedef_fun")
subprocess.run("java SemantickiAnalizator < .\\07_nedef_fun\\test.in > .\\07_nedef_fun\\mojtest.out", shell=True)
subprocess.run("FC .\\07_nedef_fun\\test.out .\\07_nedef_fun\\mojtest.out", shell=True)

print("Primjer 09_fun_povtip")
subprocess.run("java SemantickiAnalizator < .\\09_fun_povtip\\test.in > .\\09_fun_povtip\\mojtest.out", shell=True)
subprocess.run("FC .\\09_fun_povtip\\test.out .\\09_fun_povtip\\mojtest.out", shell=True)

print("Primjer 11_niz")
subprocess.run("java SemantickiAnalizator < .\\11_niz\\test.in > .\\11_niz\\mojtest.out", shell=True)
subprocess.run("FC .\\11_niz\\test.out .\\11_niz\\mojtest.out", shell=True)

print("Primjer 13_lval1")
subprocess.run("java SemantickiAnalizator < .\\13_lval1\\test.in > .\\13_lval1\\mojtest.out", shell=True)
subprocess.run("FC .\\13_lval1\\test.out .\\13_lval1\\mojtest.out", shell=True)

print("Primjer 15_cast1")
subprocess.run("java SemantickiAnalizator < .\\15_cast1\\test.in > .\\15_cast1\\mojtest.out", shell=True)
subprocess.run("FC .\\15_cast1\\test.out .\\15_cast1\\mojtest.out", shell=True)

print("Primjer 17_log")
subprocess.run("java SemantickiAnalizator < .\\17_log\\test.in > .\\17_log\\mojtest.out", shell=True)
subprocess.run("FC .\\17_log\\test.out .\\17_log\\mojtest.out", shell=True)

print("Primjer 19_cont_brk")
subprocess.run("java SemantickiAnalizator < .\\19_cont_brk\\test.in > .\\19_cont_brk\\mojtest.out", shell=True)
subprocess.run("FC .\\19_cont_brk\\test.out .\\19_cont_brk\\mojtest.out", shell=True)

print("Primjer 21_ret_nonvoid")
subprocess.run("java SemantickiAnalizator < .\\21_ret_nonvoid\\test.in > .\\21_ret_nonvoid\\mojtest.out", shell=True)
subprocess.run("FC .\\21_ret_nonvoid\\test.out .\\21_ret_nonvoid\\mojtest.out", shell=True)

print("Primjer 23_rek")
subprocess.run("java SemantickiAnalizator < .\\23_rek\\test.in > .\\23_rek\\mojtest.out", shell=True)
subprocess.run("FC .\\23_rek\\test.out .\\23_rek\\mojtest.out", shell=True)

print("Primjer 25_fun_dekl_def")
subprocess.run("java SemantickiAnalizator < .\\25_fun_dekl_def\\test.in > .\\25_fun_dekl_def\\mojtest.out", shell=True)
subprocess.run("FC .\\25_fun_dekl_def\\test.out .\\25_fun_dekl_def\\mojtest.out", shell=True)

print("Primjer 27_dekl_odmah_aktivna")
subprocess.run("java SemantickiAnalizator < .\\27_dekl_odmah_aktivna\\test.in > .\\27_dekl_odmah_aktivna\\mojtest.out", shell=True)
subprocess.run("FC .\\27_dekl_odmah_aktivna\\test.out .\\27_dekl_odmah_aktivna\\mojtest.out", shell=True)

print("Primjer 29_for")
subprocess.run("java SemantickiAnalizator < .\\29_for\\test.in > .\\29_for\\mojtest.out", shell=True)
subprocess.run("FC .\\29_for\\test.out .\\29_for\\mojtest.out", shell=True)