import java.util.*;

public class Node {

    String name;

    Node tatica;
    LinkedList<Node> djeca;

    PodatakNode podaciNodea;

    public Node(String name, Node tatica) {
        this.name = name;
        this.podaciNodea = new PodatakNode();
        this.tatica = tatica;
        this.djeca = new LinkedList<>();
    }

    //Za inicijalizaiju level = 0
    public void printFunc(int level) {
       for(int i = 0; i < level; i++) {
           System.out.print(" ");
       }
       System.out.println(name);
       for(Node n : djeca) {
         n.printFunc(level + 1);
       }
    }

    @Override
    public String toString() {
        return name + " : " + djeca.toString();
    }
}
