import subprocess

print("Skripta za testiranje - 3. labos iz PPJ")
print("22.12.2020.")

#compile SemantickiAnalizator.java
print("Compiling SemantickiAnalizator.java...")
subprocess.run(["javac", "SemantickiAnalizator.java"], shell=True)

print("\n")

#Dohvacanje imena foldera s primjerima
output = subprocess.run("dir", text=True, capture_output=True, shell=True)

#Split po redcima
x = output.stdout.split('\n')

#Izdvajanje redaka s potrebnim folderima
for i in range(7, len(x) - 7):
    #Split po razmaku
    y = x[i].split()
    
    if len(y) >= 4:
        if y[3] == "<DIR>":
            #Ispis
            print("Primjer " + y[4])
            #Pokretanje java programa nad podatcima
            subprocess.run(["java", "SemantickiAnalizator", "<", ".\\" + y[4] + "\\test.in", ">", ".\\" + y[4] + "\\mojtest.out"], shell=True)
            #Usporedba fileova
            subprocess.run(["FC", ".\\" + y[4] + "\\test.out", ".\\" + y[4] + "\\mojtest.out"], shell=True)

