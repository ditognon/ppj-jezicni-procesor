import java.util.*;
import java.io.*;
import java.nio.charset.Charset;

//Klasa koja sluzi za testiranje pojedinih funkcija i funkcionalnosti Semantickog Analizatora
public class Test {
    
    static String pZ(Node zavrsni) {
		String[] temp = zavrsni.name.split(" ");
		if (temp.length > 1) {
			return temp[0] + "(" + temp[1] + "," + temp[2] + ")";
		}
		return "\nNije predan zavrsni znak: " + zavrsni.name + "\n";
    }
    
    static boolean provjeriNizZnakova(String niz) {
        char[] znakovi = niz.toCharArray();
        boolean prefiksiran = false;

		for (int i = 0; i < znakovi.length; i++) {
			if(prefiksiran) {
                prefiksiran = false;
                if(znakovi[i] == 't' || znakovi[i] == 'n' || znakovi[i] == '0' || znakovi[i] == '\'' ||
                znakovi[i] == '\"' || znakovi[i] == '\\') {

                }else {
                    return false;
                }
            }else {
                if(znakovi[i] == '\\') prefiksiran = true;
                else {
                    if(!Charset.forName("US-ASCII").newEncoder().canEncode(Character.toString(znakovi[i]))) {
                        return false;
                    }
                }
            }
		}

		return true;
	}

    public static void main(String args[]) {
        /* Node node = new Node("L_ZAGRADA 67 (", null);
        String niz = "abeceda";
        System.out.println("Rezultat funkcije: " + provjeriNizZnakova(niz));
        System.out.println(niz); */


        try {
            Integer.parseInt("-2147483648");
            System.out.println("Moze se parsirati");
        } catch (Exception e) {
            System.out.println("Ne moze se parsirati");
        }


    }
}
