import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Stack;
import java.lang.ClassLoader;

public class SA {
    public static void main(String[] args) throws IOException {
        String ucitanRedak;
        Stack<Node> childrenList = new Stack<>();
        ArrayList<String> sinkZnakovi = new ArrayList<>();
        HashMap<String, String> redukcije = new HashMap<>();
        HashMap<String, HashMap<String, String>> tablicaAN = new HashMap<>();
        ArrayList<String> ulazniZnak = new ArrayList<>();
        ArrayList<String> printZnak = new ArrayList<>();
        Stack<String> stog = new Stack<>();

        try {
            String[] splitter;
            BufferedReader br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("./tablica.txt")));

            //ucitavanje sink. znakova
            sinkZnakovi = new ArrayList<>(Arrays.asList(br.readLine().split(" ")));

            //ucitavanje redukcija
            ucitanRedak = br.readLine();
            while(!ucitanRedak.equals("!!")) {
                splitter = ucitanRedak.split("%");
                redukcije.put(splitter[0], splitter[1]);
                ucitanRedak = br.readLine();
            }

            //ucitavanje tablica A i N
            String[] ulazniParamteri;
            String[] akcija;

            ucitanRedak = br.readLine();
            HashMap<String, String> innerAN;
            while(ucitanRedak != null) {
                splitter = ucitanRedak.split("->");
                ulazniParamteri = splitter[0].split(",");

                if(tablicaAN.get(ulazniParamteri[0]) != null) {
                    tablicaAN.get(ulazniParamteri[0]).put(ulazniParamteri[1], splitter[1]);
                } else {
                    innerAN = new HashMap<String, String>();
                    innerAN.put(ulazniParamteri[1], splitter[1]);
                    tablicaAN.put(ulazniParamteri[0], innerAN);
                }
                ucitanRedak = br.readLine();
            }

            //ucitavanje uniformnih znakova
            BufferedReader inputRead = new BufferedReader(new InputStreamReader(System.in));
            ucitanRedak = inputRead.readLine();
            while(ucitanRedak != null) {
                printZnak.add(ucitanRedak);
                ulazniZnak.add(ucitanRedak.split(" ")[0]);

                ucitanRedak = inputRead.readLine();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        ulazniZnak.add("~~");
        printZnak.add("~~");
        
        //inicijalizacija stoga
        stog.push("@@@");
        stog.push("0");

        int counter = 0;
        String[] akcijaSplitter, redukcijaSplitter;
        String redukcijaHolder, stogTop, ulazniNiz, rezultat = "";

        while(!rezultat.equals("Prv")) {
            stogTop = stog.peek();
            ulazniNiz = ulazniZnak.get(counter);

            if(tablicaAN.get(stogTop).containsKey(ulazniNiz)) {
                    rezultat = tablicaAN.get(stogTop).get(ulazniNiz);
                    akcijaSplitter = rezultat.split("#");
        
                    if(akcijaSplitter.length > 1) {
                        if(akcijaSplitter[0].equals("P")) {
                            System.err.print("Pokrenuta produkcija P : " + akcijaSplitter[1]);

                            childrenList.push(new Node(printZnak.get(counter), ulazniZnak.get(counter)));

                            stog.push(ulazniNiz);
                            stog.push(akcijaSplitter[1]);
                            counter++;
                            System.err.println(" | Stog => " + stog);
                        } else if (akcijaSplitter[0].equals("R")) {
                            System.err.print("Pokrenuta akcija R : " + akcijaSplitter[1]);
                            redukcijaSplitter = redukcije.get(akcijaSplitter[1]).split("->");

                            System.err.print(" | " + redukcijaSplitter[0] + " -> " + redukcijaSplitter[1] + "| ");

                            LinkedList<Node> addChildLista = new LinkedList<>();
                            Node inputNode = new Node(redukcijaSplitter[0], redukcijaSplitter[0]);
                            Stack<Node> stackReverser = new Stack<>();

                            int redAmmount = redukcijaSplitter[1].split(" ").length;
                            
                            if(!redukcijaSplitter[1].equals("$")) {
                                for(int i = 0; i < redAmmount; i++) {
                                    stackReverser.add(childrenList.pop());
                                }
                                for(int i = 0; i < redAmmount; i++) {
                                    addChildLista.add(stackReverser.pop());
                                }
                                inputNode.djeca = addChildLista;
    
                                childrenList.push(inputNode);
                            } else {
                                inputNode.djeca = new LinkedList<>();
                                inputNode.djeca.add(new Node("$", "$"));
                                childrenList.push(inputNode);
                            }

                            if(!redukcijaSplitter[1].equals("$")) {
                                for(int i = 0; i < redAmmount; i++) {
                                    stog.pop();
                                    stog.pop();
                                }
                            } 
                            String tempValue = tablicaAN.get(stog.peek()).get(redukcijaSplitter[0]).split("#")[1];
                            stog.push(redukcijaSplitter[0]);
                            stog.push(tempValue);
                            System.err.println(" Stog => " + stog);
                        }
                    } else {
                        System.err.println("------Prihvacen niz------");
                        rezultat = akcijaSplitter[0];
                    } 
            } else {

                System.err.println("\n------- Podaci o gresci -------");

                //printanje pogreske
                if(!printZnak.get(counter).equals("~~")) {
                    String printHelper[] = printZnak.get(counter).split(" ");
                    String printString[];
                    StringBuilder sb = new StringBuilder();

                    sb.append("Broj retka greske: " + printHelper[1] + "\n");
                    sb.append("Ocekivani uniformi znakovi: ");
                    for(String s : tablicaAN.get(stogTop).values()) {
                        sb.append(s + " ");
                     }
                    printString = printZnak.get(counter).split(" ");
                    sb.append("Procitani uniformni znak : " + printString[0] + "\nZnakovni prikaz : ");

                    for(int i = 2; i < printString.length; i++) {
                        sb.append(printString[i]);
                    }

                    System.err.println(sb.toString());

                    while(!sinkZnakovi.contains(ulazniNiz) && !ulazniNiz.equals("~~")) {
                        ulazniNiz = ulazniZnak.get(++counter);
                    }

                    if(ulazniNiz.equals("~~")) {
                        System.err.println("Nije pronadjen sinkronizacijski znak");
                        break;
                    }

                    while(!tablicaAN.get(stog.peek()).containsKey(ulazniNiz) && !stog.peek().equals("@@@")) {
                        stog.pop();
                        stog.pop();
                    } 
                    if(stog.peek().equals("@@@")) {
                        System.err.println("Nije pronadjeno stanje na stogu za sinkronizacijski znak");
                        break;
                    }
                } else {
                    System.err.println("Nedovoljno znakova, nemoguc oporavak");
                    System.err.println("-------Kraj podataka o gresci-------\n");
                    break;
                }
              System.err.println("-------Kraj podataka o gresci-------\n");
            }
        }
        System.err.println("Peek-ispis-stoga: " + childrenList.peek());
        System.err.println("Ispis-stog: " + childrenList);
        childrenList.peek().printFunc(0);
    }
}
