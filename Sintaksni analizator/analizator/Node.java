import java.io.*;
import java.util.LinkedList;


public class Node {

    String nodeName;
    String shortName;
    public LinkedList<Node> djeca;

    public Node(String name, String id) {
        nodeName = name;
        shortName = id;
        djeca = new LinkedList<>();
    }

    public void printFunc(int level) {
       for(int i = 0; i < level; i++) {
           System.out.print(" ");
       }
       System.out.println(nodeName);
       for(Node n : djeca) {
         n.printFunc(level + 1);
       }
    }

    @Override
    public String toString() {
        return shortName + " : " + djeca.toString();
    }
}
