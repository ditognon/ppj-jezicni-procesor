import java.util.*;

public class DKA<T extends Stavka> {
    private HashMap<Stanje<T>, HashMap<String, Stanje<T>>> prijelazi;
    private HashSet<Stanje<T>> stanja;
    private Stanje<T> pocetnoStanje;
    private int brojacStanja;

    public DKA() {
        prijelazi = new HashMap<Stanje<T>, HashMap<String, Stanje<T>>>();
        stanja = new HashSet<Stanje<T>>();
        brojacStanja = 0;
    }

    public void setPocetnoStanje(Stanje<T> pocetnoStanje) {
        this.pocetnoStanje = pocetnoStanje;
    }

    public Stanje<T> getStanje(Stanje<T> t) {
        for(Stanje<T> v : stanja) {
            if(t.equals(v)) {
                return v;
            }
        }
        return null;
    }

    public Stanje<T> getPocetnoStanje() {
        return pocetnoStanje;
    }

    public HashMap<Stanje<T>, HashMap<String, Stanje<T>>> getPrijelazi() {
        return prijelazi;
    }

    public HashSet<Stanje<T>> getStanja() {
        return stanja;
    }

    public boolean dodajStanje(Stanje<T> stanje) {
        if(this.stanja.contains(stanje)) {
            return false;
        }else {
            stanje.setBrojStanja(brojacStanja++);
            stanja.add(stanje);
            return true;
        }
    }

    public void dodajPrijelaz(Stanje<T> stanjeA, Stanje<T> stanjeB, String znak) {
        if(prijelazi.containsKey(stanjeA)) {
            prijelazi.get(stanjeA).put(znak, stanjeB);
        }else {
            HashMap<String, Stanje<T>> temp = new HashMap<String, Stanje<T>>();
            temp.put(znak, stanjeB);
            prijelazi.put(stanjeA, temp);
        }
    }

    public void printDKA() {
        for(Stanje<T> st : prijelazi.keySet()) {
            for(String s : prijelazi.get(st).keySet()) {
                System.err.println(st.getBrojStanja() + ", " + s + " -> " + prijelazi.get(st).get(s).getBrojStanja());
            }
        }
    }

    public void printStanjaDKA() {
        for(Stanje<T> st : stanja) {
            System.err.println("STANJE: " + st.getBrojStanja());
            System.err.println("Sadrzi potpunu: " + st.getSadrziPotpunuStavku());
            for(T t : st.getStanja()) {
                System.err.println(t + " odPocetne: " + t.getOdPocetne());
            }
            System.err.println();
        }
    }

    private void ispisiTablicu(HashMap<Integer, HashMap<String, String>> tablica) {
        for(Integer i : tablica.keySet()) {
            System.err.println(i + " =======> ");
            for(String s : tablica.get(i).keySet()) {
                System.err.println(s + " -> " + tablica.get(i).get(s));
            }
        }
        System.err.println();
    }


    //Normalna ispravna
    public HashMap<Integer, HashMap<String, String>> izgradiTablicu(ArrayList<String> zavrsni, ArrayList<String> nezavrsni) {
        HashMap<Integer, HashMap<String, String>> tablica = new HashMap<Integer, HashMap<String, String>>();

        //Akcije pomakni i stavi
       for(Stanje<T> st : this.stanja) {
           if(prijelazi.containsKey(st)) {
               for(String s : prijelazi.get(st).keySet()) {
                    if(zavrsni.contains(s)) {
                        if(tablica.containsKey(st.getBrojStanja())) {
                            tablica.get(st.getBrojStanja()).put(s, "P" + "#" + prijelazi.get(st).get(s).getBrojStanja());
                        }else {
                            HashMap<String, String> temp = new HashMap<String, String>();
                            temp.put(s, "P" + "#" + prijelazi.get(st).get(s).getBrojStanja());
                            tablica.put(st.getBrojStanja(), temp);
                        }
                    }else {
                        if(tablica.containsKey(st.getBrojStanja())) {
                            tablica.get(st.getBrojStanja()).put(s, "S" + "#" + prijelazi.get(st).get(s).getBrojStanja());
                        }else {
                            HashMap<String, String> temp = new HashMap<String, String>();
                            temp.put(s, "S" + "#" + prijelazi.get(st).get(s).getBrojStanja());
                            tablica.put(st.getBrojStanja(), temp);
                        }
                    }
               }
           }
       }

       //akcije reduciraj i prihvati
       for(Stanje<T> st : stanja) {
           if(st.getSadrziPotpunuStavku()) {
                for(Stavka stavka : st.getPotpuneStavke()) {
                    for(String s : stavka.getZnakovi()) {
                        if(tablica.containsKey(st.getBrojStanja())) {
                            if(!tablica.get(st.getBrojStanja()).containsKey(s)) {
                                if(stavka.getOdPocetne()) {
                                    tablica.get(st.getBrojStanja()).put(s, "Prv");
                                }else {
                                    tablica.get(st.getBrojStanja()).put(s, "R" + "#" + stavka.getIzvorna().getRedniBroj());
                                }
                            }
                            //Pomakni/Reduciraj proturjecje - ide u korist Pomakni
                            //Reduciraj/Reduciraj proturjecje - ide u korist produkcije s manjim rednim brojem
                            else {
                                System.err.println("Usli smo u proturjecje");
                                String string = tablica.get(st.getBrojStanja()).get(s);
                                if(string.startsWith("R")) {
                                    System.err.println("Za " + st.getBrojStanja() + " i znak " + s + " bilo je " + string);
                                    if(Integer.parseInt(string.substring(2)) > stavka.getIzvorna().getRedniBroj()) {
                                        System.err.println("PROMIJENILO SE");
                                        tablica.get(st.getBrojStanja()).replace(s, "R" + "#" + stavka.getIzvorna().getRedniBroj());
                                    }
                                }
                            }
                        }else {
                            HashMap<String, String> temp = new HashMap<String, String>();
                            if(stavka.getOdPocetne()) {
                                temp.put(s, "Prv");
                            }else {
                                temp.put(s, "R" + "#" + stavka.getIzvorna().getRedniBroj());
                            }
                            tablica.put(st.getBrojStanja(), temp);
                        }
                    }
                }
           }
       }

       return tablica;
    }

}
