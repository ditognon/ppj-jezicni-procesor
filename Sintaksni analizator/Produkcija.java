import java.util.*;

public class Produkcija {
    private String lijevaStrana;
    private ArrayList<String> desnaStrana;
    private int redniBroj;


    Produkcija(String lijevaStrana, ArrayList<String> desnaStrana, int redniBroj) {
        this.lijevaStrana = lijevaStrana;
        this.desnaStrana = desnaStrana;
        this.redniBroj = redniBroj;
    }

    public ArrayList<String> getDesnaStrana() {
        return desnaStrana;
    }

    public int getRedniBroj() {
        return redniBroj;
    }

    public String getLijevaStrana() {
        return lijevaStrana;
    }

    @Override
    public String toString() {
        return redniBroj + ". " + lijevaStrana + " -> " + desnaStrana;
    }
}
