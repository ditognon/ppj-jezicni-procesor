import java.util.*;

public class Stanje<T> {
    private HashSet<T> stanja;
    private int brojStanja;
    private boolean sadrziPotpunuStavku;
    private HashSet<Stavka> potpuneStavke;

    public Stanje(HashSet<T> stanja) {
        this.stanja = stanja;
        this.potpuneStavke = new HashSet<Stavka>();
        sadrziPotpunuStavku = false;
    }

    public HashSet<T> getStanja() {
        return stanja;
    }

    public HashSet<Stavka> getPotpuneStavke() {
        return potpuneStavke;
    }

    public boolean getSadrziPotpunuStavku() {
        return this.sadrziPotpunuStavku;
    }

    public void setSadrziPotpunuStavku(Stavka s) {
        this.sadrziPotpunuStavku = true;
        potpuneStavke.add(s);
    }

    public void setSadrziPotpunuStavku(HashSet<Stavka> hs) {
        this.sadrziPotpunuStavku = true;
        for(Stavka stavka : hs) {
            potpuneStavke.add(stavka);
        }
    }

    public void setBrojStanja(int brojStanja) {
        this.brojStanja = brojStanja;
    }

    public int getBrojStanja() {
        return brojStanja;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Stanje<?>) {
            Stanje<T> temp = (Stanje<T>) obj;
            return this.stanja.equals(temp.stanja);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return stanja.hashCode();
    }

    public static void main(String args[]) {
        HashSet<String> ss = new HashSet<String>();
        HashSet<String> pp = new HashSet<String>();

        ss.add("alfa");
        ss.add("omega");
        ss.add("kiwi");

        pp.add("omega");
        pp.add("alfa");

        Stanje<String> s = new Stanje<String>(ss);
        Stanje<String> p = new Stanje<String>(pp);

        System.err.println(s.equals(p));
    }
}
