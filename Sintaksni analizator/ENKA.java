import java.util.*;

// $ oznacava epsilon prijelaz
public class ENKA <T extends Stavka> {
    private T pocetnoStanje;
    private HashMap<T, HashMap<String, HashSet<T>>> prijelazi;
    private HashSet<T> stanja;
    private ArrayList<T> prihvatljiva;
    private HashMap<T, Vector<T>> epsilonOkruzenja;

    public ENKA() {
        prijelazi = new HashMap<T, HashMap<String, HashSet<T>>>();
        prihvatljiva = new ArrayList<T>();
        stanja = new HashSet<T>();
        epsilonOkruzenja = new HashMap<>();
    }

    public void setPocetnoStanje(T pocetnoStanje) {
        this.pocetnoStanje = pocetnoStanje;
    }

    public T getStanje(T stanje) {
        for(T t : stanja) {
            if(t.equals(stanje)) {
                return t;
            }
        }
        return null;
    }

    public boolean dodajStanje(T stanje) {
        if(stanja.contains(stanje)) {
            return false;
        }else {
            stanja.add(stanje);
            return true;
        }
    }

    public HashSet<T> getStanja() {
        return stanja;
    }

    public void dodajPrijelaz(T stanjeA, T stanjeB, String znak) {
        if(prijelazi.get(stanjeA) != null) {
            if(prijelazi.get(stanjeA).get(znak) != null) {
                prijelazi.get(stanjeA).get(znak).add(stanjeB);
            }else {
                HashSet<T> temp = new HashSet<T>();
                temp.add(stanjeB);
                prijelazi.get(stanjeA).put(znak, temp);
            }
        }else {
            HashSet<T> tempSet = new HashSet<T>();
            tempSet.add(stanjeB);
            HashMap<String, HashSet<T>> tempMap = new HashMap<String, HashSet<T>>();
            tempMap.put(znak, tempSet);
            prijelazi.put(stanjeA, tempMap);
        }
    }

    public void dodajEpsilonPrijelaz(T stanjeA, T stanjeB) {
        dodajPrijelaz(stanjeA, stanjeB, "$");
    }

    public void printENKA() {
        for(T t : prijelazi.keySet()) {
            System.err.println("Iz stanja " + t + " ::::");
            for(String a : prijelazi.get(t).keySet()) {
                System.err.print(" sa znakom " + a + " ======>>>>> ");
                for(T s : prijelazi.get(t).get(a)) {
                    System.err.println(s);
                }
            }
            System.err.println();
        }
    }

    private void izracunajEpsilonOkruzenja() {
        HashSet<T> stanjaZaEpsilon = new HashSet<T>();
        stanjaZaEpsilon.addAll(stanja);

        //Pronadi sva stanja u koja se ne prelazi s epsilon prijelazom
        for(T radnoStanje : stanja) {
            if(prijelazi.containsKey(radnoStanje)) {
                for(String s : prijelazi.get(radnoStanje).keySet()) {
                    if(s.equals("$")) {
                        stanjaZaEpsilon.remove(radnoStanje);
                    }
                }
            }
        }

        for(T radnoStanje : stanjaZaEpsilon) {
            eOkruzenje(radnoStanje);
        }
    }

    private Stanje<T> eOkruzenje(T stanje) {
        Vector<T> y = new Vector<T>();
        Stack<T> stack = new Stack<T>();

        stack.push(stanje);
        y.add(stanje);

        while(!stack.isEmpty()) {
            T t = stack.pop();
            if(epsilonOkruzenja.containsKey(t)) {
                y.addAll(epsilonOkruzenja.get(t));
            }else {
                if(prijelazi.containsKey(t)) {
                    if(prijelazi.get(t).containsKey("$")) {
                        for(T tt : prijelazi.get(t).get("$")) {
                            if(!y.contains(tt)) {
                                y.add(tt);
                                stack.push(tt);
                            }
                        }
                    }
                }
            }
        }

        epsilonOkruzenja.put(stanje, y);

        HashSet<T> temp = new HashSet<T>();
        temp.addAll(y);
        return new Stanje<T>(temp);
    }

    private HashSet<T> eOkruzenje2(T stanje) {
        Vector<T> y = new Vector<T>();
        Stack<T> stack = new Stack<T>();

        stack.push(stanje);
        y.add(stanje);

        while(!stack.isEmpty()) {
            T t = stack.pop();
            if(prijelazi.containsKey(t)) {
                if(prijelazi.get(t).containsKey("$")) {
                    for(T tt : prijelazi.get(t).get("$")) {
                        if(!y.contains(tt)) {
                            y.add(tt);
                            stack.push(tt);
                        }
                    }
                }
            }
        }

        HashSet<T> temp = new HashSet<T>();
        temp.addAll(y);
        return temp;
    }

    //bolja verzija funkcije sa vekotorom i stogom
    private Stanje<T> eOkruzenje3(T stanje) {
        Vector<T> y = new Vector<T>();
        Stack<T> stack = new Stack<T>();

        stack.push(stanje);
        y.add(stanje);

        while(!stack.isEmpty()) {
            T t = stack.pop();
            if(prijelazi.containsKey(t)) {
                if(prijelazi.get(t).containsKey("$")) {
                    for(T tt : prijelazi.get(t).get("$")) {
                        if(!y.contains(tt)) {
                            y.add(tt);
                            stack.push(tt);
                        }
                    }
                }
            }
        }

        HashSet<T> temp = new HashSet<T>();
        temp.addAll(y);
        return new Stanje<T>(temp);
    }

    private void obradiStanje(Stanje<T> stanje) {
        for(T t : stanje.getStanja()) {
            if(t.getPotpuna()) {
                stanje.setSadrziPotpunuStavku(t);
            }
        }
    }
    
    public DKA<T> izgradiDKA(ArrayList<String> zavrsni, ArrayList<String> nezavrsni) {
        DKA<T> automat = new DKA<T>();

        HashSet<String> sviZnakovi = new HashSet<String>();
        sviZnakovi.addAll(zavrsni);
        sviZnakovi.addAll(nezavrsni);

        //Mozda je potrebno da bude HashSet - problem izvlacenja iz seta
        Stack<Stanje<T>> neobradenaStanja = new Stack<Stanje<T>>();

        //1. korak algoritma, pocetno stanje dka je e-okruzenje pocetnog stanja enka
        Stanje<T> temp = eOkruzenje(this.pocetnoStanje);
        obradiStanje(temp);
        automat.dodajStanje(temp);
        automat.setPocetnoStanje(temp);

        Stanje<T> trenutno;
        neobradenaStanja.push(temp);

        do {

            //Uzima prvo neobradeno stanje
            trenutno = neobradenaStanja.pop();

            //2. korak algoritma
            //Za svaki znak(zavrsni i nezavrsni)
            for(String znak : sviZnakovi) {
                //razmislit o ucinku na memoriju
                HashSet<T> setOdredisnihStanjaEnkaZaZnak = new HashSet<T>();
                //Gledamo u koji skup stanja enka prelazi skup stanja enka trenutnog stanja dka
                for(T stanjeEnka : trenutno.getStanja()) {
                    if(prijelazi.containsKey(stanjeEnka)) {
                        if(prijelazi.get(stanjeEnka).containsKey(znak)) {
                            setOdredisnihStanjaEnkaZaZnak.addAll(prijelazi.get(stanjeEnka).get(znak));
                        }
                    }
                }

                if(!setOdredisnihStanjaEnkaZaZnak.isEmpty()) {
                    //Gradimo eOkolinu seta odredisnih stanja i od te okoline radimo novo stanje
                    //u koje se prelazi sa trenutnim znakom iz trenutnog stanja

                    //razmislit o ucinku na memoriju
                    HashSet<T> pomocni = new HashSet<T>();
                    for(T t : setOdredisnihStanjaEnkaZaZnak) {
                        pomocni.addAll(eOkruzenje2(t));
                    }

                    Stanje<T> novoStanje = new Stanje<T>(pomocni);
                    obradiStanje(novoStanje);
                    //Potencijalni problem u slucaju da postoji vec identicno stanje stanju koje dodajemo
                    if(automat.dodajStanje(novoStanje)) {
                        automat.dodajPrijelaz(trenutno, novoStanje, znak);
                        neobradenaStanja.push(novoStanje);
                    }else {
                        automat.dodajPrijelaz(trenutno, automat.getStanje(novoStanje), znak);
                    }
                }
                
            }

        }while(!neobradenaStanja.isEmpty());
        
        return automat;
    }

    //Ne rekurzivna funkcija - Ispravnija
    /* public DKA<T> izgradiDKA2() {
        DKA<T> automat = new DKA<T>();
        HashMap<T, Stanje<T>> eOkruzenja = new HashMap<T, Stanje<T>>();

        //postavi pocetno stanje automata
        automat.setPocetnoStanje(eOkruzenje(this.pocetnoStanje));

        //pregledaj je li pocetno stanje potpuno ili zavrsno
        for(T t : automat.getPocetnoStanje().getStanja()) {
            if(t.getPotpuna()) {
                automat.getPocetnoStanje().setSadrziPotpunuStavku(t);
                if(t.getOdPocetne()) {
                    automat.getPocetnoStanje().setSadrziZavrsnu();
                }
            }
        }

        //pocetno stanje ima oznaku 0
        automat.dodajStanje(automat.getPocetnoStanje());

        Stanje<T> radno;
        HashSet<Stanje<T>> help = new HashSet<Stanje<T>>();
        ArrayList<Stanje<T>> neobradenaStanja = new ArrayList<Stanje<T>>();
        neobradenaStanja.add(automat.getPocetnoStanje());
        help.add(automat.getPocetnoStanje());
        while(!neobradenaStanja.isEmpty()) {
            radno = neobradenaStanja.get(0);
            for(T t : radno.getStanja()) {
                if(prijelazi.containsKey(t)) {
                    for(String s : prijelazi.get(t).keySet()) {
                        if(s != "$") {
                            for(T tt : prijelazi.get(t).get(s)) {
                                Stanje<T> temp;
                                if(eOkruzenja.containsKey(tt)) {
                                    temp = eOkruzenja.get(tt);
                                }else {
                                    temp = eOkruzenje(tt);
                                    eOkruzenja.put(tt, temp);
                                }

                                //Ispitujemo dal kompleksno stanje dka sadrzi potpunu stavku ili stavku koja je proizasla iz pocetna
                                for(T ttt : temp.getStanja()) {
                                    if(ttt.getPotpuna()) {
                                        temp.setSadrziPotpunuStavku(ttt);
                                        if(ttt.getOdPocetne()) {
                                            temp.setSadrziZavrsnu();
                                        }
                                    }
                                }

                                //Ako automat vec ima to stanje
                                if(automat.getStanja().contains(temp)) {
                                    if(automat.getPrijelazi().containsKey(radno)) {
                                        if(!automat.getPrijelazi().get(radno).containsKey(s)) {
                                            automat.dodajPrijelaz(radno, automat.getStanje(temp), s);
                                        }
                                    }else {
                                        automat.dodajPrijelaz(radno, automat.getStanje(temp), s);
                                    }
                                }
                                //Ako automat vec ima prijelaz iz trenutnog stanja za trenutni znak
                                else if(automat.getPrijelazi().containsKey(radno) && automat.getPrijelazi().get(radno).containsKey(s)) {
                                    automat.getPrijelazi().get(radno).get(s).getStanja().addAll(temp.getStanja());
                                    if(temp.getSadrziPotpunuStavku()) {
                                        automat.getPrijelazi().get(radno).get(s).setSadrziPotpunuStavku(temp.getPotpuneStavke());
                                    }
                                }
                                else {
                                    automat.dodajStanje(temp);
                                    automat.dodajPrijelaz(radno, temp, s);
                                    if(!help.contains(temp)) {
                                        neobradenaStanja.add(temp);
                                        help.add(temp);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            neobradenaStanja.remove(radno);
            help.remove(radno);
        }

        return automat;
    } */



    public static void main(String args[]) {

    }
}
