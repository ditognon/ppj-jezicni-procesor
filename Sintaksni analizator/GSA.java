import java.io.*;
import java.util.*;

public class GSA {
    private static String pocetniZnak;
    public static final String tocka = "~TOCKA~";
    private static ArrayList<String> nezavrsni = new ArrayList<String>();
    private static ArrayList<String> zavrsni = new ArrayList<String>();
    private static ArrayList<String> sinkronizacijski = new ArrayList<String>();
    private static HashMap<String, ArrayList<Produkcija>> produkcije = new HashMap<String, ArrayList<Produkcija>>();
    private static HashMap<String, HashSet<String>> zapocinjeZnakom = new HashMap<String, HashSet<String>>();
    private static HashMap<String, HashSet<String>> zapocinjeIzravno = new HashMap<String, HashSet<String>>();
    private static HashMap<String, HashSet<String>> zapocinje = new HashMap<String, HashSet<String>>();
    private static HashSet<String> prazniZnakovi = new HashSet<String>();
    private static int brojilo = 0;

    private static void ucitajPodatke() {
        String redak;
		//Ucitavanje podataka iz ulazne datoteke
		try {
            brojilo = 1;
            //Za debugging
            //ClassLoader.getSystemResourceAsStream("./mojprimjer/test.san")
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            redak = br.readLine();
            String key = null;
			while(redak != null) {
                //Ucitavanje nezavrsnih znakova
                if(redak.startsWith("%V")) {
                    String temp[] = redak.split(" ");
                    for(int i = 1; i < temp.length; i++) {
                        nezavrsni.add(temp[i]);
                    }
                }
                
                //Ucitavanje zavrsnih znakova
                else if(redak.startsWith("%T")) {
                    String temp[] = redak.split(" ");
                    for(int i = 1; i < temp.length; i++) {
                        zavrsni.add(temp[i]);
                    }
                }
                
                //Ucitavanje sinkronizacijskih znakova
                else if(redak.startsWith("%Syn")){
                    String temp[] = redak.split(" ");
                    for(int i = 1; i < temp.length; i++) {
                        sinkronizacijski.add(temp[i]);
                    }
                }

                //Ucitavanje produkcija gramatike
                else {
                    if(redak.startsWith("<")) {
                        key = redak;
                    }else if(redak.startsWith(" ")) {
                        //brojilo++;
                        ArrayList<String> lista = new ArrayList<String>();
                        String array[] = redak.split(" ");
                        for(int i = 1; i < array.length; i++) {
                            lista.add(array[i]);
                        }

                        if(produkcije.containsKey(key)) {
                            produkcije.get(key).add(new Produkcija(key, lista, brojilo++));
                        }else {
                            ArrayList<Produkcija> temp = new ArrayList<Produkcija>();
                            temp.add(new Produkcija(key, lista, brojilo++));
                            produkcije.put(key, temp);
                        }
                    }else {
                        System.out.println("Doslo je do pogreske tijekom ucitavanja produkcija gramatike");
                    }
                }

                redak = br.readLine();
			}
		}catch(IOException e) {
			e.printStackTrace();
        }

        pocetniZnak = nezavrsni.get(0);
    }

    private static void ispisiPodatke() {
        System.out.println("================= NEZAVRSNI ZNAKOVI GRAMATIKE ==================");
        for(String a : nezavrsni) {
            System.out.println(a);
        }
        System.out.println();

        System.out.println("================= ZAVRSNI ZNAKOVI GRAMATIKE ====================");
        for(String a : zavrsni) {
            System.out.println(a);
        }
        System.out.println();

        System.out.println("=================== SINKRONIZACIJSKI ZNAKOVI ======================");
        for(String a : sinkronizacijski) {
            System.out.println(a);
        }
        System.out.println();

        System.out.println("===================== PRODUKCIJE GRAMATIKE ========================");
        for(String a : produkcije.keySet()) {
            System.out.print(a + " => ");
            for(Produkcija b : produkcije.get(a)) {
                //System.out.print(b.getRedniBroj() + ". ");
                for(String c : b.getDesnaStrana()) {
                    System.out.print(c + " ");
                }
                System.out.print(" | ");
            }
            System.out.println();
        }
    }

    private static void izracunajPrazneZnakove() {
        for(String s : nezavrsni) {
            for(Produkcija p : produkcije.get(s)) {
                if(p.getDesnaStrana().get(0).equals("$")) {
                    prazniZnakovi.add(s);
                    break;
                }
            }
        }
    }

    private static void izracunajZapocinjeIzravno() {
        //Prvo odredujemo prazne znakove
        izracunajPrazneZnakove();

        for(String s : nezavrsni) {
            HashSet<String> tempS = new HashSet<String>();
            for(Produkcija p : produkcije.get(s)) {
                for(String a : p.getDesnaStrana()) {
                    if(zavrsni.contains(a)) {
                        tempS.add(a);
                        break;
                    }else if(nezavrsni.contains(a)){
                        if(prazniZnakovi.contains(a)) {
                            tempS.add(a);
                            continue;
                        }else {
                            tempS.add(a);
                            break;
                        }
                    }
                }
            }
            zapocinjeIzravno.put(s, tempS);
        }
    }

    // Tranzitivno i refleksivno okruzenje relacije zapocinjeIzravnoZnakom
    private static void izracunajZapocinjeZnakom() {
        //Prvo racunamo relaciju zapocinjeIzravnoZnakom
        izracunajZapocinjeIzravno();

        //prekopiramo sve iz zapocinje izravno u zapocinje
        for(String s : zapocinjeIzravno.keySet()) {
            HashSet<String> temp = new HashSet<String>();
            temp.addAll(zapocinjeIzravno.get(s));
            zapocinjeZnakom.put(s, temp);
        }

        //Refleksivnost za zavrsne znakove
        for(String s : zavrsni) {
            HashSet<String> temp = new HashSet<String>();
            temp.add(s);
            zapocinjeZnakom.put(s, temp);
        }
        
        //nezavrsni znakovi
        boolean promjena = true;
        while(promjena == true) {
            promjena = false;
            for(String s : nezavrsni) {
                HashSet<String> temp = new HashSet<String>();
                if(zapocinjeZnakom.containsKey(s)) {
                    for(String ss : zapocinjeZnakom.get(s)) {
                        if(zapocinjeZnakom.containsKey(ss)) {
                            for(String sss : zapocinjeZnakom.get(ss)) {
                                if(!zapocinjeZnakom.get(s).contains(sss)) {
                                    temp.add(sss);
                                    promjena = true;
                                }
                            }
                        }
                    }
                    zapocinjeZnakom.get(s).addAll(temp);
                }
            }
        }

        // nezavrsni znakovi refleksivnost
        for(String s : nezavrsni) {
            if(zapocinjeZnakom.containsKey(s)) {
                zapocinjeZnakom.get(s).add(s);
            }else {
                HashSet<String> temp = new HashSet<String>();
                temp.add(s);
                zapocinjeZnakom.put(s, temp);
            }
        }
    }  

    private static void izracunajZapocinje() {
        izracunajZapocinjeZnakom();

        for(String s : zapocinjeZnakom.keySet()) {
            for(String ss : zapocinjeZnakom.get(s)) {
                if(zavrsni.contains(ss)) {
                    if(zapocinje.containsKey(s)) {
                        zapocinje.get(s).add(ss);
                    }else {
                        HashSet<String> temp = new HashSet<String>();
                        temp.add(ss);
                        zapocinje.put(s, temp);
                    }
                }
            }
        }
    }


    private static void obradiStavku(Stavka radna, ENKA<Stavka> automat) {
        Stavka zamjena;
        //Ako nije potpuna LR1 stavka
        if(radna.desnoOdTocke().size() >= 1) {
            String znakDodTocke = radna.desnoOdTocke().get(0);
            // Ako je znak desno od tocke nezavrsni znak
            if(nezavrsni.contains(znakDodTocke)) {
                HashSet<String> znakovi = new HashSet<String>();
                if(radna.desnoOdTocke().size() > 1) {
                    for(int i = 1; i < radna.desnoOdTocke().size(); i++) {
                        znakovi.addAll(zapocinje.get(radna.desnoOdTocke().get(i)));
                        if(!prazniZnakovi.contains(radna.desnoOdTocke().get(i))) {
                            break;
                        }else {
                            if(i == radna.desnoOdTocke().size() - 1) {
                                znakovi.addAll(radna.getZnakovi());
                            }
                        }
                    }
                }else {
                    znakovi.addAll(radna.getZnakovi());
                }
                
                for(Produkcija p : produkcije.get(znakDodTocke)) {
                    Stavka temp = new Stavka(p.getLijevaStrana(), p.getDesnaStrana(), znakovi, 0, p);
                    if(automat.getStanja().contains(temp)) {
                        automat.dodajEpsilonPrijelaz(radna, automat.getStanje(temp));
                    }else {
                        automat.dodajStanje(temp);
                        automat.dodajEpsilonPrijelaz(radna, temp);
                        obradiStavku(temp, automat);
                    }
                }
                zamjena = radna.sljedecaStavka();
                if(automat.dodajStanje(zamjena)) {
                    automat.dodajPrijelaz(radna, zamjena, znakDodTocke);
                    obradiStavku(zamjena, automat);
                }else {
                    automat.dodajPrijelaz(radna, automat.getStanje(zamjena), znakDodTocke);
                }
               
            } 
            // Ako je znak desno od tocke zavrsni znak
            else if(zavrsni.contains(znakDodTocke)){
                zamjena = radna.sljedecaStavka();
                if(automat.dodajStanje(zamjena)) {
                    automat.dodajPrijelaz(radna, zamjena, znakDodTocke);
                    obradiStavku(zamjena, automat);
                }else {
                    automat.dodajPrijelaz(radna, automat.getStanje(zamjena), znakDodTocke);
                }
                
                
            }
        }
        //ako je potpuna LR stavka
        else {
            radna.setPotpuna(true);
        }
    }

    // ~~ oznacava oznaku kraja niza
    private static ENKA<Stavka> izgradiENKA() {
        izracunajZapocinje();

        Stavka nulta = new Stavka("q0", null, null, null);

        //Stvori novi ENKA i postavi pocetnu stavku za pocetno stanje
        ENKA<Stavka> automat = new ENKA<Stavka>();
        automat.dodajStanje(nulta);
        automat.setPocetnoStanje(nulta);

        HashSet<String> tempSet = new HashSet<String>();
        tempSet.add("~~");

        for(Produkcija p : produkcije.get(pocetniZnak)) {
            Stavka radna = new Stavka(p.getLijevaStrana(), p.getDesnaStrana(), tempSet, 0, p);
            if(!automat.dodajStanje(radna)) {
                automat.dodajEpsilonPrijelaz(nulta, automat.getStanje(radna));
            }else {
                automat.dodajEpsilonPrijelaz(nulta, radna);
            }

            //Potencijalno nepotrebno
            obradiStavku(radna, automat);
        }

        boolean temp = false;
        for(Stavka a : automat.getStanja()) {
            temp = false;
            if(a.getPotpuna()) {
                if(a.getLijevaStrana().equals(pocetniZnak)) {
                    temp = true;
                    for(String s : a.getZnakovi()) {
                        if(!s.equals("~~")) {
                            temp = false;
                        }
                    }
                }
            }
            a.setOdPocetne(temp);
        }

        return automat;
    }

    //generira datoteku ./tablica.txt
    public static void generirajTablicu(HashMap<Integer, HashMap<String, String>> tablica) {
        try {
			String imeDatoteke = "./analizator/tablica.txt";
			File datoteka = new File(imeDatoteke);
			datoteka.createNewFile();
			FileWriter fw = new FileWriter(imeDatoteke);
            
            //Ispis sinkronizacijskih znakova odvojenih zarezom
            for(int i = 0; i < sinkronizacijski.size(); i++) {
                if(i == sinkronizacijski.size() -  1) {
                    fw.write(sinkronizacijski.get(i));
                }else {
                    fw.write(sinkronizacijski.get(i) + " ");
                }
            }

            fw.write("\n");

            //Ispis produkcija s rednim brojem u formatu 1%S->A b c D
            for(String s : produkcije.keySet()) {
                for(Produkcija p : produkcije.get(s)) {
                    fw.write(p.getRedniBroj() + "%" + p.getLijevaStrana() + "->");
                    for(int i = 0; i < p.getDesnaStrana().size(); i++) {
                        if(i == p.getDesnaStrana().size() - 1) {
                            fw.write(p.getDesnaStrana().get(i));
                        }else {
                            fw.write(p.getDesnaStrana().get(i) + " ");
                        }
                    } 
                    /* for(String ss : p.getDesnaStrana()) {
                        fw.write(ss + " ");
                    } */
                    fw.write("\n");
                }
            }

            fw.write("!!\n");

            for(Integer i : tablica.keySet()) {
                for(String s : tablica.get(i).keySet()) {
                    fw.write(i + "," + s + "->" + tablica.get(i).get(s));
                    fw.write("\n");
                }
            }
            
			fw.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
    }

    private static void dodajPocetnoStanjeGramatike() {
        String novoPocetnoStanje = new String("<#JASAMNOVOSTANJE#>");
        
        ArrayList<String> tempListStringova = new ArrayList<String>();
        tempListStringova.add(pocetniZnak);
        Produkcija p = new Produkcija(novoPocetnoStanje, tempListStringova, brojilo++);

        ArrayList<Produkcija> tempListProdukcija = new ArrayList<Produkcija>();
        tempListProdukcija.add(p);
        produkcije.put(novoPocetnoStanje, tempListProdukcija);

        nezavrsni.add(novoPocetnoStanje);

        //POTENCIJALNI PROBLEM
        pocetniZnak = novoPocetnoStanje;
    }


    public static void main(String args[]) {
        ucitajPodatke();
        //ispisiPodatke();

        dodajPocetnoStanjeGramatike();

        ENKA<Stavka> enka = izgradiENKA();

        DKA<Stavka> dka = enka.izgradiDKA(zavrsni, nezavrsni);

        //dka.printDKA(); 

        //dka.printStanjaDKA();

        generirajTablicu(dka.izgradiTablicu(zavrsni, nezavrsni));
    }
}
