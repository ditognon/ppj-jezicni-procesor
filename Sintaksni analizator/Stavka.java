import java.util.*;

public class Stavka {
    public static final String tocka = "~TOCKA~";
    private String lijevaStrana;
    private ArrayList<String> desnaStrana;
    private HashSet<String> znakovi;
    private Produkcija izvorna;
    private boolean potpuna;
    private boolean odPocetne;

    public Stavka(String lijevaStrana, ArrayList<String> desnaStrana, HashSet<String> znakovi, int indeksTocka, Produkcija izvorna) {
        this.lijevaStrana = lijevaStrana;
        this.znakovi = znakovi;
        this.potpuna = false;
        this.odPocetne = false;
        this.desnaStrana = new ArrayList<String>();
        if(!desnaStrana.get(0).equals("$")) {
            this.desnaStrana.addAll(desnaStrana);
        }
        this.desnaStrana.add(indeksTocka, tocka);
        this.izvorna = izvorna;
    }

    public Stavka(String lijevaStrana, ArrayList<String> desnaStrana, HashSet<String> znakovi, Produkcija izvorna) {
        this.lijevaStrana = lijevaStrana;
        this.znakovi = znakovi;
        this.potpuna = false;
        this.desnaStrana = desnaStrana;
        this.izvorna = izvorna;
    }

    public boolean getOdPocetne() {
        return this.odPocetne;
    }

    public void setOdPocetne(boolean odPocetne) {
        this.odPocetne = odPocetne;
    }

    public boolean getPotpuna() {
        return potpuna;
    }

    public void setPotpuna(boolean potpuna) {
        this.potpuna = potpuna;
    }

    public ArrayList<String> getDesnaStrana() {
        return desnaStrana;
    }

    public Produkcija getIzvorna() {
        return izvorna;
    }
    
    public String getLijevaStrana() {
        return lijevaStrana;
    }

    public HashSet<String> getZnakovi() {
        return znakovi;
    }
    
    public ArrayList<String> desnoOdTocke() {
        ArrayList<String> temp = new ArrayList<String>();
        int indeks = desnaStrana.indexOf(tocka);
        for(int i = indeks + 1; i < desnaStrana.size(); i++) {
            temp.add(desnaStrana.get(i));
        }

        return temp;
    }

    // Vraca novu stavku s tockom pomaknutom za jedno mjesto u desno
    // S -> ~TOCKA~ A B {~~} ===>  S -> A ~TOCKA~ B {~~}
    public Stavka sljedecaStavka() {
        if(this.desnaStrana.indexOf(tocka) < this.desnaStrana.size() - 1) {
            int indeks = this.desnaStrana.indexOf(tocka);
            ArrayList<String> tempList = new ArrayList<String>();
            tempList.addAll(this.desnaStrana);
            tempList.remove(indeks);
            tempList.add(indeks + 1, tocka);
            return new Stavka(this.lijevaStrana, tempList, this.znakovi, this.izvorna);
        }else {
            System.err.println("Tocka je vec na krajnje desnom mjestu");
            return this;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Stavka) {
            Stavka temp = (Stavka)obj;
            return this.lijevaStrana.equals(temp.lijevaStrana) && this.desnaStrana.equals(temp.desnaStrana) && 
            this.znakovi.equals(temp.znakovi);
        }
        return false;
    }

    @Override
    public int hashCode() {
        if(desnaStrana == null) {
            return 666;
        }else {
            return lijevaStrana.hashCode() + desnaStrana.hashCode() + znakovi.hashCode();
        }
        
    }

    @Override
    public String toString() {
        if(desnaStrana == null) {
            return lijevaStrana;
        }else {
            return lijevaStrana + " -> " + desnaStrana.toString() + "   {" + znakovi.toString() + "}";
        }
    }

    public static void main(String args[]) {
        /* ArrayList<String> poc1 = new ArrayList<String>();
        poc1.add("B");
        poc1.add("C");
        HashSet<String> poc2 = new HashSet<String>();
        poc2.add("~~");
        Stavka test = new Stavka("A", poc1, poc2, 1);
        System.err.println("Pocetna stavka:");
        System.err.println(test);
        System.err.println("Sljedeca stavka:");
        System.err.println(test.sljedecaStavka()); */
        
    }
}
