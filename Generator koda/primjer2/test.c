#include <stdio.h>

int x = -5;
int y = 6;
int a = 0;
int main(void) {
	a = x * y;
	printf("%d\n", a);
	x = 5;
	y = -6;
	a = x * y;
	printf("%d\n", a);
	x = -5;
	a = x * y;
	printf("%d\n", a);
	
	a = x / y;
	printf("%d\n", a);
	x = 5;
	a = x / y;
	printf("%d\n", a);
	x = -5;
	y = 6;
	a = x / y;
	printf("%d\n", a);

	a = x % y;
	printf("%d\n", a);
	x = 5;
	y = -6;
	a = x % y;
	printf("%d\n", a);
	x = 5;
	y = 4;
	a = x % y;
	printf("%d\n", a);
	
    return x;
}
