import java.util.*;

class TablicaZnakova {
    static int brojilo = 0;
    public HashMap<String, String> deklariraneVar;
    public HashMap<String, String> vrijednostiVar;
    public TablicaZnakova roditelj;
    public int id;

    public TablicaZnakova(TablicaZnakova roditelj) {
        this.deklariraneVar = new HashMap<String, String>();
        this.vrijednostiVar = new HashMap<String, String>();
        this.roditelj = roditelj;
        id = ++brojilo;
    }
}