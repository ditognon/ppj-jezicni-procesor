import java.util.ArrayList;

public class PodatakNode {
    String ime;
    String tip;
    String brElem;
    Integer lIzraz;
    String ntip;
    ArrayList<String> tipovi;
    ArrayList<String> imena; 

    public PodatakNode() {
        tipovi = new ArrayList<String>();
        imena = new ArrayList<String>();
        ime = null;
        tip = null;
        brElem = null;
        lIzraz = null;
        ntip = null;
    }
}
