import java.util.HashMap;
import java.util.Stack;
import java.util.Vector;
import java.util.ArrayList;

public class Automat {
    public int brStanja = 0;
    public boolean init = false;
    public int vectorChooser = 0;
    public int pocetnoStanje;
    public int prihvatljivoStanje;
    public String regDef;
    Stack<Integer> st = new Stack<>();
    ArrayList<Vector<Integer>> alVec = new ArrayList<>();
    ArrayList<String> akcija = new ArrayList<>();
    public HashMap<StanjeZnak, ArrayList<Integer>> tablicaPrijelaza = new HashMap<StanjeZnak, ArrayList<Integer>>();

    public int novoStanje() {
        this.brStanja++;
		return this.brStanja - 1;
    }

    //Stvara za zadani automat epsilon prijelaz od stanjeOd do stanjeDo
    public void dodajEpsilonPrijelaz(int stanjeOd, int stanjeDo) {
        this.dodajPrijelaz(stanjeOd, "~EPSILON~", stanjeDo);
    }

    public void dodajPrijelaz(int stanjeOd, String znak, int stanjeDo) {
        if((this.brStanja < stanjeOd + 1) && (this.brStanja < stanjeDo + 1)) {
            System.err.println("NE POSTOJI STANJE ZA KOJE SE POKUSAVA DODATI PRIJELAZ");
        }else {
            StanjeZnak sz = new StanjeZnak(stanjeOd, znak);
            if(!this.tablicaPrijelaza.containsKey(sz)) {
                ArrayList<Integer> temp = new ArrayList<Integer>();
                temp.add(Integer.valueOf(stanjeDo));
                this.tablicaPrijelaza.put(sz, temp);
            }else {
                this.tablicaPrijelaza.get(sz).add(stanjeDo);
            }
        }
    }

    public Automat() {
        alVec.add(new Vector<>());
        alVec.add(new Vector<>());
    }

    public void resetSimulaciju() {
        alVec.get(0).clear();
        alVec.get(1).clear();
        init = false;
    }

    public void eOkolina(String ulZnak) {
        Vector<Integer> getVector;
        Vector<Integer> otherVector;
        StanjeZnak sz = new StanjeZnak();
        StanjeZnak vSz = new StanjeZnak();
        ArrayList<Integer> naStogStanja;
        Integer stogStanje;

        getVector = alVec.get(vectorChooser);
        otherVector = alVec.get(1 - vectorChooser);

        otherVector.clear();

        if(!init) {
            init = true;
            otherVector.add(pocetnoStanje);
        }
        
        for(Integer stanjePocVek : getVector) {
            sz.stanje = stanjePocVek;
            sz.znak = ulZnak;
            
            ArrayList<Integer> al = tablicaPrijelaza.get(sz);
            if(al != null) {
                for(Integer i : al) {
                    otherVector.add(i);
                }
            }
        }
        //dodavanje stanja na stack
        for(Integer i : otherVector) {
            st.push(i);
        }

        while(!st.isEmpty()) {
            stogStanje = st.pop();

            vSz.stanje = stogStanje;
            vSz.znak = "~EPSILON~";

            naStogStanja = tablicaPrijelaza.get(vSz);
            if(naStogStanja != null) {
                for(Integer i : naStogStanja) {
                    if(!otherVector.contains(i)) {
                        otherVector.add(i);
                        st.push(i);
                    }
                }
            }  
        }
        vectorChooser = 1 - vectorChooser;
    }

    public int provjeriZnak(String znak) {
        eOkolina(znak);
        for(Integer i : alVec.get(vectorChooser)) {
            if(i.equals(prihvatljivoStanje)) {
                return 2;
            }
        }
        if(alVec.get(vectorChooser).isEmpty()) {
            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Regdef: " + regDef + " ");
        sb.append("Akcije: ");
        for(String s : akcija) {
            sb.append(s + " ");
        }
        sb.append(tablicaPrijelaza);

        return sb.toString();
    }
}
