import java.util.List;

public class RegexPravilo {
    public String regex;
    public List<String> pravila;

    public RegexPravilo() {}

    public RegexPravilo(String regex, List<String> pravila) {
        this.regex = regex;
        this.pravila = pravila;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof RegexPravilo) {
			RegexPravilo temp = (RegexPravilo)o;
			return this.regex.equals(temp.regex) && pravila.equals(temp.pravila);
		}
		return false;
    }

    @Override
    public int hashCode() {
        return regex.hashCode() + pravila.hashCode();
    }

}
 