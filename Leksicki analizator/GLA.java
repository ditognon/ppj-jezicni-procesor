import java.io.*;
import java.util.HashMap;
import java.util.ArrayList;

class GLA {	
	static HashMap<String, String> regularniIzrazi = new HashMap<String, String>();
	static String[] stanjaLA;
	static String[] imenaLJ;
	static HashMap<String, ArrayList<RegexPravilo>> pravila = new HashMap<String, ArrayList<RegexPravilo>>();

	//Ispisuje sve podatkovne GLA na pregledan nacin
	static void ispisPodatkovnihStruktura() {
		//Provjera parsiranja ulaza
		System.err.println("REGULARNI IZRAZI");
		for(String s : regularniIzrazi.keySet()) {
			System.out.print(s + " ");
			System.err.println(regularniIzrazi.get(s));
		}

		System.err.println();

		System.err.println("STANJA LEKSICKOG ANALIZATORA");
		for(int i = 0; i < stanjaLA.length; i++) {
			System.err.println(stanjaLA[i]);
		}

		System.err.println();

		System.err.println("IMENA LEKSICKIH JEDINKI");
		for(int i = 0; i < imenaLJ.length; i++) {
			System.err.println(imenaLJ[i]);
		}

		System.err.println();

		System.err.println("PRAVILA LEKSICKOG ANALIZATORA");
		for(String s : pravila.keySet()) {
			System.err.println(s + " ---------------------- ");
			for(RegexPravilo p : pravila.get(s)) {
				System.out.print(p.regex + ": ");
				for(String a : p.pravila) {
					System.out.print(a + " ");
				}
				System.err.println();
			}
			System.err.println();
		}
	}

	//Provjerava je li na zadanom indeksu stvarno operator ili je prefiksiran
	static boolean jeOperator(String izraz, int i) {
		int brojilo = 0;
		while(((i - 1) >= 0) && izraz.charAt(i - 1) == '\\') {
			brojilo++;
			i--;
		}

		return brojilo % 2 == 0;
	}

	//Uzima regularnu definiciju i vraca regularni izraz 
	static String pretvoriRegularnuDefiniciju(String definicija) {
		String replace;
        int brZagrada = 0;
        int prviIndeks = 0;
        char niz[] = definicija.toCharArray();
        for(int i = 0; i < niz.length; i++) {
            if(niz[i] == '{' && jeOperator(definicija, i)) {
                brZagrada++;
                prviIndeks = i;
			}else if(niz[i] == '}' && jeOperator(definicija, i) && brZagrada > 1) {
                brZagrada--;
			}
			else if(brZagrada == 1 && niz[i] == '}' && jeOperator(definicija, i)) {
                String naziv = definicija.substring(prviIndeks + 1, i);
                replace = regularniIzrazi.get(naziv);
                String temp = definicija.substring(0, prviIndeks) + "(" + replace + ")";
                if(i + 1 < definicija.length()) {
                    definicija = temp + definicija.substring(i + 1);
                }else {
                    definicija = temp;
                }
                brZagrada = 0;
                niz = definicija.toCharArray();
                prviIndeks = 0;
            }
        }
         
		return definicija;
	}

	//Obraduje jednu po jednu regularnu definiciju
	static void obradiRegularneDefinicije() {
		for(String s : regularniIzrazi.keySet()) {
			String temp = regularniIzrazi.get(s);
			regularniIzrazi.replace(s, pretvoriRegularnuDefiniciju(temp));
		}

		for(String s : pravila.keySet()) {
			for(RegexPravilo p : pravila.get(s)) {
				p.regex = pretvoriRegularnuDefiniciju(p.regex);
			}
		}
	}

	//Rekurzivna funkcija za pretvaranje regularnog izraza u ENKA
	static ParStanja pretvori(String izraz, Automat a) {
		//1.korak Rasjecakti regularni izraz na podizraze odijeljene sa |
		ArrayList<String> podDijelovi = new ArrayList<String>();
        int brZagrada = 0;
        int k = 0;
		for(char c : izraz.toCharArray()) {
			if(c == '(' && jeOperator(izraz, k)) {
                brZagrada++;
			}else if(c == ')' && jeOperator(izraz, k)) {
                brZagrada--;
			}
			//Ako je pronaden operator | grupiraj lijevi dio niza u zasebni regularni izraz
			else if(brZagrada == 0 && c == '|' && jeOperator(izraz, k)) {
                podDijelovi.add(izraz.substring(0, k));
                izraz = izraz.substring(k + 1);
                k = -1;
            }
            k++;
		}
		//Ako je bilo grupiranja grupiraj desni dio niza u zasebni regularni izraz
		if(!podDijelovi.isEmpty()) {
			podDijelovi.add(izraz);
        }
		
		//Nastavak nakon pocetnog cjepanja u odnosu na |
		int lijevoStanje = a.novoStanje(); 
		int desnoStanje = a.novoStanje();

		if(!podDijelovi.isEmpty()) {
			for(String dio : podDijelovi) {
				ParStanja temp = pretvori(dio, a);
				a.dodajEpsilonPrijelaz(lijevoStanje, temp.lijevoStanje);
				a.dodajEpsilonPrijelaz(temp.desnoStanje, desnoStanje);
			}
		}else {
			boolean prefiksirano = false;
			//zadnjeStanje predstavlja najdesnije stanje ENKA koje je izgradeno do sada
			int zadnjeStanje = lijevoStanje;
			char[] niz = izraz.toCharArray();
			for(int i = 0; i < niz.length; i++) {
				int aa, bb;
				if(prefiksirano) {
					prefiksirano = false;
					String prZnak;
					if(niz[i] == 't') prZnak = "\\t";
					else if(niz[i] == 'n') prZnak = "\\n";
					else if(niz[i] == '_') prZnak = " ";
					else prZnak = String.valueOf(niz[i]);

					aa = a.novoStanje();
					bb = a.novoStanje();

					a.dodajPrijelaz(aa, prZnak, bb);
				}else {
					if(niz[i] == '\\') { 
						prefiksirano = true;
						continue;
					}
					if(niz[i] != '(') {
						aa = a.novoStanje();
						bb = a.novoStanje();
						if(niz[i] == '$') {
							a.dodajEpsilonPrijelaz(aa, bb);
						}else {
							a.dodajPrijelaz(aa, String.valueOf(niz[i]), bb);
						}
					}else {
						int balans = 0;
						int j;
						for(j = i + 1; j < niz.length; j++) {
							if(niz[j] == '(' && jeOperator(izraz, j)) {
								balans++;
							}else if(niz[j] == ')' && jeOperator(izraz, j) && balans != 0) {
								balans--;
							}else if(niz[j] == ')' && jeOperator(izraz, j) && balans == 0) {
								break;
							}
						}
						ParStanja temp = pretvori(izraz.substring(i + 1, j),a);
						aa = temp.lijevoStanje;
						bb = temp.desnoStanje;
						i = j;
					}
				}

				//Provjera ponavljanja
				if(((i + 1) < niz.length) && (niz[i + 1] == '*')) { 
					int x = aa;
					int y = bb;
					aa = a.novoStanje();
					bb = a.novoStanje();
					a.dodajEpsilonPrijelaz(aa, x);
					a.dodajEpsilonPrijelaz(y, bb);
					a.dodajEpsilonPrijelaz(aa, bb);
					a.dodajEpsilonPrijelaz(y, x);
					i++;
				}

				a.dodajEpsilonPrijelaz(zadnjeStanje, aa);
				zadnjeStanje = bb;
			}
			a.dodajEpsilonPrijelaz(zadnjeStanje, desnoStanje);
		}
		return new ParStanja(lijevoStanje, desnoStanje);
	}

	//Gradi automat za zadani regularni izraz
	public static Automat izgradiENKA(String izraz) {
		Automat a = new Automat();
		ParStanja par = pretvori(izraz, a);
		a.pocetnoStanje = par.lijevoStanje;
		a.prihvatljivoStanje = par.desnoStanje;

		return a;
	}

	public static void stvoriTablicu() {

		try {
			String imeDatoteke = "./analizator/tablica.txt";
			File datoteka = new File(imeDatoteke);
			datoteka.createNewFile();
			FileWriter fw = new FileWriter(imeDatoteke);
			fw.write(stanjaLA[0]);
			fw.write("\n");
			Automat a;
			int brojacStanja = 0;
			int brojacRegDef;
			for(String s : pravila.keySet()) {
				fw.write(s);
				fw.write("\n");
				brojacRegDef = 0;
				for(RegexPravilo pr : pravila.get(s)) {
					a = izgradiENKA(pr.regex);
					fw.write(pr.regex);
					fw.write("\n");
					fw.write(String.valueOf(a.pocetnoStanje));
					fw.write("\n");
					fw.write(String.valueOf(a.prihvatljivoStanje));
					fw.write("\n");
					StringBuilder akcije = new StringBuilder();
					for(String akcija : pr.pravila) {
						akcije.append(akcija);
						akcije.append("%%");
					}
					akcije.delete(akcije.length() - 2, akcije.length());
					fw.write(akcije.toString());
					fw.write("\n");

					//Pisanje tablice prijelaza
					for(StanjeZnak sz : a.tablicaPrijelaza.keySet()) {
						StringBuilder sb = new StringBuilder();

						for(Integer akcija : a.tablicaPrijelaza.get(sz)) {
							sb.append(akcija);
							sb.append(",");
						}
						sb.deleteCharAt(sb.length() - 1);
						fw.write(sz.stanje + "," + sz.znak + "->" + sb.toString());
						fw.write("\n");
					}
					//Brine se da se ne isprinta viska na kraju
					if(brojacRegDef < pravila.get(s).size() - 1) {
						fw.write("!!");
						fw.write("\n");
					}
					brojacRegDef++;
				}
				//Brine se da se ne isprinta viska na kraju
				if(brojacStanja < pravila.keySet().size() - 1) {
					fw.write("??");
					fw.write("\n");
				}
				brojacStanja++;
			}
			fw.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}

	public static void stvoriLA() {
		try {
			String imeDatoteke = "./analizator/LA.java";
			File datoteka = new File(imeDatoteke);
			datoteka.createNewFile();
			FileWriter fw = new FileWriter(imeDatoteke);
			BufferedReader br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("./analizator/helpLA.txt")));
			String redak = br.readLine();
			while(redak != null) {
				fw.write(redak);
				fw.write("\n");
				redak = br.readLine();
			}
			fw.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		
	}

    public static void main(String args[]) {
		String redak;
		//Ucitavanje podataka iz ulazne datoteke
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			redak = br.readLine();
			while(redak != null) {

				//Ucitavanje Regularnih definicija
				if(redak.startsWith("{") && stanjaLA == null) {
					regularniIzrazi.put(redak.substring(1, redak.indexOf("}")), redak.split(" ")[1]);
				}

				//Ucitavanje Stanja Leksickog analizatora
				else if(redak.startsWith("%X")) {
					String temp;
					temp = redak.substring(3);
					stanjaLA = temp.split(" ");
				}

				//Ucitavanje imena leksickih jedinki
				else if(redak.startsWith("%L")) {
					String temp;
					temp = redak.substring(3);
					imenaLJ = temp.split(" ");

				//Ucitavanje pravila leksickog analizatora
				}else if(redak.startsWith("<")){
					String kljuc = redak.substring(1, redak.indexOf(">"));
					String tempRegex = redak.substring(redak.indexOf(">") + 1);

					//Punjenje liste za pravila
					ArrayList<String> tempPravila = new ArrayList<String>();
					redak = br.readLine();
					while(redak != null) {
						if(redak.startsWith("{")) {
							redak = br.readLine();
							continue;
						}
						else if(redak.startsWith("}")) {
							break;
						}
						tempPravila.add(redak);
						redak = br.readLine();
					}


					if(pravila.containsKey(kljuc)) {
						pravila.get(kljuc).add(new RegexPravilo(tempRegex, tempPravila));
					}else {
						ArrayList<RegexPravilo> tempLista = new ArrayList<RegexPravilo>();
						tempLista.add(new RegexPravilo(tempRegex, tempPravila));
						pravila.put(kljuc, tempLista);
					}

				}else {
					System.err.println("Doslo je do greske");
				}
				redak = br.readLine();
			}
		}catch(IOException e) {
			e.printStackTrace();
		}

		//Pretvara regularne definicije u regularne izraze
		obradiRegularneDefinicije();

		//Stavara ./analizator/tablica.txt
		stvoriTablicu();

		stvoriLA();
    }
}