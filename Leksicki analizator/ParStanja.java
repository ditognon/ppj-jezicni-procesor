public class ParStanja {
   public int lijevoStanje;
   public int desnoStanje;
   
   public ParStanja() {}

   public ParStanja(int lijevoStanje, int desnoStanje) {
        this.lijevoStanje = lijevoStanje;
        this.desnoStanje = desnoStanje;
   }

   @Override
   public boolean equals(Object o) {
       if(o instanceof ParStanja) {
            ParStanja temp = (ParStanja)o;
            return this.lijevoStanje == temp.lijevoStanje && this.desnoStanje == temp.desnoStanje;
       }
       return false;
   }

   @Override
   public int hashCode() {
       return Integer.valueOf(lijevoStanje).hashCode() + Integer.valueOf(desnoStanje).hashCode();
   }
}
