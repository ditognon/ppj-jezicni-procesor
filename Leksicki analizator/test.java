import java.util.regex.*;
import java.util.ArrayList;

public class test {

    static boolean jeOperator(String izraz, int i) {
		int brojilo = 0;
		while(((i - 1) >= 0) && izraz.charAt(i - 1) == '\\') {
            brojilo++;
			i--;
		}

		return brojilo % 2 == 0;
    }

    // STARA VERZIJA OVE FUNKCIJE - Sigurno ispravna
    /* static String pretvoriRegularnuDefiniciju(String definicija) {
		String nazivRegex;

		Pattern regDefUzorak = Pattern.compile("\\{\\w*\\}");
		Matcher matcher = regDefUzorak.matcher(definicija);
		while(matcher.find()) {
			nazivRegex = matcher.group();
			nazivRegex = nazivRegex.substring(1, nazivRegex.length() - 1);
			String novi = definicija.substring(0, matcher.start()) + "(" + regularniIzrazi.get(nazivRegex) + ")" + 
			definicija.substring(matcher.end()); 
			definicija = novi;
			matcher = regDefUzorak.matcher(definicija);
		}
		return definicija;
	} */

    /* static void pretvori(String izraz) {
		//1.korak Rasjecakti regularni izraz na podizraze odijeljene sa |
		ArrayList<String> podDijelovi = new ArrayList<String>();
        int brZagrada = 0;
        int i = 0;
		for(char c : izraz.toCharArray()) {
			if(c == '(' && jeOperator(izraz, i)) {
                brZagrada++;
			}else if(c == ')' && jeOperator(izraz, i)) {
                brZagrada--;
			}
            //Ako je pronaden operator | grupiraj lijevi dio niza u zasebni regularni izraz
			else if(brZagrada == 0 && c == '|' && jeOperator(izraz, i)) {
                podDijelovi.add(izraz.substring(0, i));
                izraz = izraz.substring(i + 1);
                i = -1;
            }
            i++;
		}
		//Ako je bilo grupiranja grupiraj desni dio niza u zasebni regularni izraz
		if(!podDijelovi.isEmpty()) {
			podDijelovi.add(izraz);
        }
        
        System.out.println("Dijelovi: ");
        for(String s : podDijelovi) {
            System.out.println(s);
        }
    } */

    //Nova verzija ove funkcije - vjerovatno ispravna - POTREBNO DETALJNO TESTIRANJE
    static String pretvoriRegularnuDefiniciju(String definicija) {
		String replace = "1|2|3|4|5|6|7|8|9";
        int brZagrada = 0;
        int prviIndeks = 0;
        char niz[] = definicija.toCharArray();
        for(int i = 0; i < niz.length; i++) {
            if(niz[i] == '{' && jeOperator(definicija, i)) {
                brZagrada++;
                prviIndeks = i;
			}else if(niz[i] == '}' && jeOperator(definicija, i) && brZagrada > 1) {
                brZagrada--;
			}
			else if(brZagrada == 1 && niz[i] == '}' && jeOperator(definicija, i)) {
                String naziv = definicija.substring(prviIndeks + 1, i);
                //replace = regularniIzrazi.get(naziv);
                String temp = definicija.substring(0, prviIndeks) + "(" + replace + ")";
                if(i + 1 < definicija.length()) {
                    definicija = temp + definicija.substring(i + 1);
                }else {
                    definicija = temp;
                }
                brZagrada = 0;
                niz = definicija.toCharArray();
                prviIndeks = 0;
            }
        }
         
		return definicija;
	}

    public static void main(String args[]) {
        /* String definicija = "(e|E)($|+|-){znamenka}{znamenka}*";
        Pattern regDefUzorak = Pattern.compile("\\{\\w*\\}"); 
		Matcher matcher = regDefUzorak.matcher(definicija);
		if(matcher.find()) {
            String uzorak = matcher.group();
			System.out.println();
            System.out.println("Naden regex u : " + definicija);
            System.out.println(uzorak);
			System.out.println();
		}else {
            System.out.println("Not found!");
        } */
        //String s ="(0|1|2|3|4|5|6|7|8|9)(0|1|2|3|4|5|6|7|8|9)*.(0|1|2|3|4|5|6|7|8|9)*($|((e|E)($|+|-)(0|1|2|3|4|5|6|7|8|9)(0|1|2|3|4|5|6|7|8|9)*))";
        String s = "{znamenka}{znamenka}*|0x{hexZnamenka}{hexZnamenka}*";
        System.out.println(s);
        System.out.println(pretvoriRegularnuDefiniciju(s));
    }
}
