import java.io.*;
import java.lang.ClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

public class LA {
    //ucitavanje tablice.txt
    public static void main(String[] args) throws IOException {
        int pocetak = 0, zavrsetak = 0, posljedni = 0; 
        String lineRead;
        String glPocetnoStanje = "";
        String glTrenutnoStanje;
        String stanjeRead;
        String[] prijelazSplit;
        String[] ulazniZnakovi;
        String[] novaStanja;
        HashMap<String, LinkedList<Automat>> listaAutomata = new HashMap<>();
        
        
    try {
        BufferedReader br = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("./tablica.txt")));
        glPocetnoStanje = br.readLine();
            LinkedList<Automat> linAut = new LinkedList<>();
            do {
              stanjeRead = br.readLine();
              linAut = new LinkedList<>();
              Automat at = new Automat();
              do {
                at = new Automat();
                at.regDef = br.readLine();

                at.pocetnoStanje = Integer.parseInt(br.readLine());
                at.prihvatljivoStanje = Integer.parseInt(br.readLine());
                
                at.akcija = new ArrayList<>(Arrays.asList(br.readLine().split("%%")));
                StanjeZnak sz = new StanjeZnak();;
                ArrayList<Integer> al = new ArrayList<>();;
                lineRead = br.readLine();
                while(lineRead != null && !lineRead.equals("!!") && !lineRead.equals("??")) {
                    prijelazSplit = lineRead.split("->");
                    ulazniZnakovi = prijelazSplit[0].split(",");
                    novaStanja = prijelazSplit[1].split(",");

                    sz = new StanjeZnak();
                    
                    sz.stanje = Integer.parseInt(ulazniZnakovi[0]);

                    if(lineRead.contains(",,")) {
                      sz.znak = ",";
                    } else {
                      sz.znak = ulazniZnakovi[1];
                    }
                    
                    al = new ArrayList<>();
                    
                    for(String s : novaStanja) {
                       al.add(Integer.parseInt(s));
                    }
                    lineRead = br.readLine();
                    at.tablicaPrijelaza.put(sz, al);
                }
                linAut.add(at);
              } while(lineRead != null && !(lineRead.startsWith("??")));
              listaAutomata.put(stanjeRead, linAut);
            } while (lineRead != null);
    } catch(IOException e) {
      e.printStackTrace();
    }

    glTrenutnoStanje = glPocetnoStanje;

    LinkedList<Automat> listaAt = new LinkedList<>();
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    ArrayList<Character> izvorniKod = new ArrayList<>();
    ArrayList<String> izvString = new ArrayList<>();
    char[] inputChar;
    String readIzvorni = br.readLine();

    while (readIzvorni != null) {
      inputChar = readIzvorni.toCharArray();
      for(char c : inputChar) {
        izvorniKod.add(c);
      }
      izvorniKod.add('\n');
      readIzvorni = br.readLine();
    };

    for(Character c : izvorniKod) {
      switch(c) {
        case '\n':
            izvString.add("\\n");
            break;
        case '\t':
            izvString.add("\\t");
            break;
        case '\b':
            izvString.add("\\b");
            break;
        case '\f': 
            izvString.add("\\f");
            break;
        case '\r':
            izvString.add("\\r");
            break;
        case '\'':
            izvString.add("\'");
            break;
        case '\"':
            izvString.add("\"");
            break;
        case '\\':
            izvString.add("\\");
            break;
        case ' ':
            izvString.add(" ");
            break;
        default:
            izvString.add(c.toString());
      }
    }

    ArrayList<String> akcije = new ArrayList<>();
    ArrayList<String> emptyAkcije = new ArrayList<>();
    int redakBr = 1;
    String msg = "random";
    boolean povecajRedak = false;
    boolean resetAutomat = true;
    int automatStatus;
    int tempStatus = -1;
    boolean taken;
    do {
       taken = false;
       automatStatus = 0;

       listaAt = listaAutomata.get(glTrenutnoStanje);
       int brojac = 1;

       if(resetAutomat) {
         for(Automat at : listaAt) {
          at.provjeriZnak("~EPSILON~");
          resetAutomat = false;
         }
      }
       for(Automat at : listaAt) {
        tempStatus = at.provjeriZnak(izvString.get(zavrsetak));
        if(tempStatus == 1 && automatStatus != 2) {
          automatStatus = tempStatus;
        } else if(tempStatus == 2 && !taken) {
          msg = at.regDef;
          taken = true;
          automatStatus = tempStatus;
          akcije = at.akcija;
        }
       }

       zavrsetak++;

       if(automatStatus == 2) {
         posljedni = zavrsetak - 1;
       }

       if(automatStatus == 0) {
         for(Automat at : listaAutomata.get(glTrenutnoStanje)) {
           at.resetSimulaciju();
           resetAutomat = true;
         }
         if(akcije.isEmpty()) {
          System.err.println("Error found, izbacen: " + izvString.get(pocetak));
          pocetak++;
          zavrsetak = pocetak;
         } else {
          StringBuilder sb = new StringBuilder();
          for(int i = pocetak; i <= posljedni; i++) {
            sb.append(izvString.get(i));
          }

          boolean vratiSe = false;
          int vratiValue = -1;

          for(String akc : akcije) {
            String[] akcPolje = akc.split(" ");
            if(akcPolje[0].equals("VRATI_SE")) {
              vratiSe = true;
              vratiValue = Integer.parseInt(akcPolje[1]);
              akc = "-";
            }
          }

        for(String akc : akcije) {
            String[] akcDef = akc.split(" ");
            if(akcDef.length == 1) {
              if(akcDef[0].equals("NOVI_REDAK")) {
                povecajRedak = true;
              }
              if(!akcDef[0].equals("-") && !akcDef[0].equals("NOVI_REDAK")) {
                if(!vratiSe) {
                  System.out.println(akcDef[0] + " " + redakBr + " " + sb.toString());
                } else {
                  StringBuilder sbSec = new StringBuilder();
                  for(int i = pocetak; i < vratiValue + pocetak; i++) {
                    sbSec.append(izvString.get(i));
                  }
                  System.out.println(akcDef[0] + " " + redakBr + " " + sbSec.toString());
                }
              }
            } else {
              if(akcDef[0].equals("UDJI_U_STANJE")) {
                glTrenutnoStanje = akcDef[1];
              }
            }      
          }

          if(vratiSe) {
            pocetak = pocetak + vratiValue;
            zavrsetak = pocetak;
            posljedni = pocetak;
            vratiSe = false;
          } else {
            pocetak = posljedni + 1;
            zavrsetak = pocetak;
          }
          

          if(povecajRedak) {
            povecajRedak = false;
            redakBr++;
          }
         }
         akcije = emptyAkcije;
       }
    } while(izvString.size() > zavrsetak);
  }
}
