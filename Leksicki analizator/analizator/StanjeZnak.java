public class StanjeZnak {
    public int stanje;
    public String znak;

    StanjeZnak() {}

    StanjeZnak(int stanje, String znak) {
        this.stanje = stanje;
        this.znak = znak;
    }

    @Override
    public boolean equals(Object o) {
        if(o instanceof StanjeZnak) {
			StanjeZnak temp = (StanjeZnak)o;
			return this.stanje == temp.stanje && this.znak.equals(temp.znak);
		}
		return false;
    }

    @Override
    public int hashCode() {
        return Integer.valueOf(stanje).hashCode() + znak.hashCode();
    }

    @Override
    public String toString() {
        return stanje + " " + znak;
    }

}
